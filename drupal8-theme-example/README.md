# Bootstrap 4 sub-theme

This build is 100% my work.   I was involved in this project from conception, design, development, deployment and continue to be involved with ongoing maintenance.  

Now, I am not the best designer and the theme reflects that.   The site however does use recommended drupal modules and follows drupal best practices for Drupal 8/9 sites.

## Features

* Composer manages Drupal Core and Module install/versions.
* Site has Drupal Configuration saved in code
* Drush is installed and other maintenance is handled by drush
* Using SVGs for some iconography
* Drupal updates and security releases are applied on a regular basis  (tested first)
* Using VIEWS to pull a custom content type and output the results by date in a table format

# Key Modules

* PARAGRAPHS have been installed and the client has been taught to use paragraphs for contact creation.
* ADVAGG installed to optimize JS/CSS delivery
