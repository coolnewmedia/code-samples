This overrides sass folder is for node/page specific overrides.

Examples:   
*   A special background for a specific node
*   A promo node with different font
*   Requirements for custom spacing on a component or paragraph on a specific node/page,


Overrides should be wrapped with the node specific class from the classes in the body tag.

eg.  
```
.node-40 {
    # write your sass changes here
}
```