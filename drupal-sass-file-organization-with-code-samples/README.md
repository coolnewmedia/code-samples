# Sass Organization for a complex Drupal site

These SASS files depend on a task runner such as Grunt or Gulp or WebPack to process the SASS into CSS.

I picked up this organization method while working for The Agency and further refined it.    THE SASS is organization loosely follows BEM standards however because multiple developers worked on the SASS at one time or another, it was hard to keep the code fully organized.

The SASS is organized from broad declarations to granular, for greater efficiency and re-usability.


