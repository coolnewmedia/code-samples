# A simple drupal 8 module to augment some functionality

## Transliteration

The contrib transliteration module does most of what we want it to do but I wanted further file sanitizing.  The augmentation changes the uploaded filename and returns the filename as all lowercase and replaces spaces with underscores.   Regular transliteration does not change the filename case.

## Links

Simply adds functionality to force external links get target="_blank".
