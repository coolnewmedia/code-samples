<?php
/**
 * Default Template
 */

// Get Context, Starting in functions.php
$context = Timber::get_context();

// Get Current Post.
$context['post'] = new Timber\Post();

// Get All Options with one query.
$context['options'] = get_fields('options');

if(isset($_GET['job_title'])) {
    $context['vars']['jobTitle'] = $_GET['job_title'];
}

if(isset($context['options']['show_related_posts']) && $context['options']['show_related_posts'] == 1) {
    $postTerms = wp_get_post_terms($context['post']->ID,'press_article_type');
    $relatedPostsTerms = array();
    for($i = 0; $i < count($postTerms); $i++) {
        $relatedPostsTerms[] = $postTerms[$i]->term_id;
    }
    $args = array(
        'post_type' => $context['post']->post_type,
        'posts_per_page' => $context['options']['related_posts_limit'],
        'post_status' => 'publish',
        'orderby' => 'rand',
        'post__not_in' => array($context['post']->ID)
    );
    if(count($postTerms) > 0) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $postTerms[0]->taxonomy,
                'field' => 'term_id',
                'terms' => $relatedPostsTerms
            )
        );
    }
    $context['related_posts'] = Timber::get_posts($args);
}

// provide rooms filtering widget some info
  $args = [
    'taxonomy' => 'room_amenities',
    'hide_empty' => false
  ];
  $context['room_amenity_terms'] = Timber::get_terms($args);

// Use Front Page.
$templates = array( 'single-' . $context['post']->post_type . '.twig', 'index.twig' );
if ( is_front_page() ) {
    $templates = array( 'home.twig' );
}
Timber::render( $templates, $context );