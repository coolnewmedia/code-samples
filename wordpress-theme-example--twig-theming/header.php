<?php
/**
 * Current Not Used. Header & Footer templates will be directly included base.twig
 */
$context = Timber::get_context();
$context['posts'] = new Timber\PostQuery();
$context['foo'] = 'bar';
$templates = array('header.twig');
Timber::render($templates, $context);
