<?php

/***************************************************************
 * Timber/Twig Check
 ***************************************************************/

if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

/***************************************************************
 * Timber/Twig Add To Context
 ***************************************************************/
function add_to_context($context) {

    global $wp;

    $context['content_dir'] = WP_CONTENT_DIR;
    $context['content_url'] = WP_CONTENT_URL;
    $context['primary_navigation'] = new \Timber\Menu('primary_navigation');
    $context['secondary_navigation'] = new \Timber\Menu('secondary_navigation');
    $context['footer_primary_navigation'] = new \Timber\Menu('footer_primary_navigation');
    $context['current_url'] = "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $context['current_ip'] = $_SERVER['SERVER_ADDR'];
    $context['weather_data'] = get_option('weather_data');
    $context['options'] = get_fields('option');

    return $context;
}

add_filter('timber_context', 'add_to_context');

/***************************************************************
 * Timber/Twig Define Custom Functions
 ***************************************************************/
function add_to_twig($twig) {

    /* this is where you can add your own functions to twig */

//    $twig->addExtension( new Twig_Extension_StringLoader() );
//    $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));

    $function_filedate = new Twig_SimpleFunction(
        'fileDate',
        /**
         * @param $file_path
         * This function generates a new file path with the last date of filechange
         * to support better better client caching via Expires header:
         * i.e:
         * css/style.css -> css/style.1428423235.css
         *
         * Usage in template files:
         * i.e:
         * <link rel="stylesheet" href="{{ fileDate('css/style.css') }}">
         *
         * Apache Rewrite Rule:
         *
         * RewriteCond %{REQUEST_FILENAME} !-f
         * RewriteCond %{REQUEST_FILENAME} !-d
         * RewriteRule ^(.*)\.[\d]{10}\.(css|js)$ $1.$2 [NC,L]
         *
         * Apache Document Root MUST be configured without the trailing slash!
         *
         * @return mixed
         */
        function ($file_path) {
            $change_date = @filemtime($_SERVER['DOCUMENT_ROOT'] . '/' . $file_path);
            if (!$change_date) {
                //Fallback if mtime could not be found:
                $change_date = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            }
            return $change_date;
        }
    );
    $twig->addFunction($function_filedate);

    $filter_parse_url_scheme = new Twig\TwigFilter(
        'parseUrlScheme',
        /**
         * @param $url
         * This function runs PHP's parse_url on URLs and returns the URL's URL scheme (http, https etc)
         *
         * @return string
         */
        function ($link_url) {
            return parse_url($link_url, PHP_URL_SCHEME);
        }
    );
    $twig->addFilter($filter_parse_url_scheme);

    $filter_parse_url_host = new Twig\TwigFilter(
        'parseUrlHost',
        /**
         * @param $url
         * This function runs PHP's parse_url on URLs and returns the URL's host
         *
         * @return string
         */
        function ($link_url) {
            return parse_url($link_url, PHP_URL_HOST);
        }
    );
    $twig->addFilter($filter_parse_url_host);

    return $twig;
}

add_filter('get_twig', 'add_to_twig');


/***************************************************************
 * Layout automation for ACF's Flexible Content
 ***************************************************************/
require_once locate_template('/acf-layout-class.php');

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point($path) {

    // update path
    $path = get_stylesheet_directory() . '/acf-json';

    // return
    return $path;
}


/***************************************************************
 * Content width
 ***************************************************************/
//$content_width is a global variable used by WordPress for max image upload sizes and media embeds (in pixels).
if (!isset($content_width)) {
    $content_width = 1280;
}


/***************************************************************
 * Register Menus
 ***************************************************************/
if (function_exists('register_nav_menus')) {
    function register_theme_menus() {
        register_nav_menus(
            array(
                'primary_navigation' => __('Primary Navigation'),
                'secondary_navigation' => __('Secondary Navigation'),
                'footer_primary_navigation' => __('Footer Primary Navigation'),
                'footer_secondary_navigation' => __('Footer Secondary Navigation')
            )
        );
    }

    add_action('init', 'register_theme_menus');
}


// Replace the Gravity Forms Spinner with transparent gif (base 64 encoded)
function gf_spinner_replace($image_src, $form) {
    return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'; // relative to you theme images folder
}

add_filter('gform_ajax_spinner_url', 'gf_spinner_replace', 10, 2);



/***************************************************************
 * General Settings page
 ***************************************************************/
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'General Settings Page',
        'menu_title' => 'General Settings',
        'menu_slug' => 'general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}


/***************************************************************
 * Additional Functions and Declarations mananged in the /includes folder
 ***************************************************************/

include "includes/image-settings.php";
include "includes/custom-post-types.php";
include "includes/custom-mce-settings.php";
include "includes/development.php";
