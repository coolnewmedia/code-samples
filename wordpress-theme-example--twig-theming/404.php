<?php
/**
 * Default Template
 */

// Get Context, Starting in functions.php
$context = Timber::get_context();

// Get Current Post.
$context['post'] = new Timber\Post();

// Get All Options with one query.
$context['options'] = get_fields('options');

// Use 404 Page.
$templates = array( '404.twig' );

Timber::render( $templates, $context );