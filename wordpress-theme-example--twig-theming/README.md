# WordPress theme with TWIG framework

This was the first project I worked on as my onboarding experience with the Agency.  The project was assigned to a lead developer (also working remote) who chose to install TWIG themeing.

The project had it set of challenges, including the lead moving on to another employer, and I was left to finish this project.  Most of my knowledge was with Drupal 7 and I had not worked with WordPress that much.  As well Advanced Custom Fields / Flexible layouts and Twig themeing were new to me.

With limited assistance, I delivered the first iteraction of this website. 

The files included here are a sample for the site build and files I worked on directly.

## During this build I learned

* Advanced Custom Fields  (Drupal knowledge was helpful here)
* A whole bunch of WordPress tricks
* Started adapting BEM SASS/CSS coding
