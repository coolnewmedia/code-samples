

<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

// Get Context, Starting in functions.php
$context = Timber::get_context();

// Get Current Post.
$context['post'] = new Timber\Post();

// Get All Options with one query.
$context['options'] = get_fields('options');


if ($context['post']->post_type == 'press_article') {
    $context['posts'] = new Timber\PostQuery(array(
        'post_type' => 'press_article',
        'posts_per_page' => $context['options']['press_limit'],
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => 1
    ));

    if(isset($context['options']['press_article_meta_description'])) {

        $context['options']['meta_description'] = $context['options']['press_article_meta_description'];

    }

} else if($context['post']->post_type == 'jobpostings') {
    if(isset($context['options']['job_archive_page_title'])) {
        $context['wp_title'] = $context['options']['job_archive_page_title'];
    }
    if(isset($context['options']['job_archive_meta_description'])) {
        $context['options']['meta_description'] = $context['options']['job_archive_meta_description'];
    }
}

// Use Front Page.
$templates = array('archive-' . $context['post']->post_type . '.twig', 'archive.twig');
Timber::render($templates, $context);

