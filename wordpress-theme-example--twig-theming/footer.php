<?php
/**
 * Current Not Used. Header & Footer templates will be directly included base.twig
 */
$context = Timber::get_context();
$context['posts'] = new Timber\PostQuery();
$context['options'] = get_fields('options');

$templates = array('footer.twig');
Timber::render($templates, $context);
?>