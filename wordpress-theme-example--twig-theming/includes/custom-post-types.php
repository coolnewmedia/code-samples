<?php
/***************************************************************
 * Example of a custom post type named "Businesses"
 ***************************************************************/

//add_action('init', 'register_businesses');
//function register_businesses() {
//	$labels = array(
//		'name' => __('Businesses'),
//		'menu_name' => __('Businesses'),
//		'singular_name' => __('Business'),
//		'add_new_item' => __('Add New Business'),
//		'edit_item' => __('Edit Business'),
//		'new_item' => __('New Business'),
//		'view_item' => __('View Business'),
//		'search_items' => __('Search Businesses'),
//		'not_found' => __('No Businesses found'),
//		'not_found_in_trash' => __('No Businesses found in Trash'),
//	);
//	$args = array(
//		'labels' => $labels,
//		'supports' => array('title','thumbnail','revisions'),
//		'rewrite' => array( 'slug' => 'businesses', 'with_front' => false ),
//		'capability_type' => 'post',
//		'menu_position' => 20, // after Pages
//		'menu_icon' => 'dashicons-id', // http://calebserna.com/dashicons-cheatsheet/
//		'hierarchical' => false,
//		'public' => false,
//		'exclude_from_search' => true,
//		'publicly_queryable' => false,
//		'show_ui' => true,
//		'query_var' => true,
//		'can_export' => true,
//	);
//	register_post_type( 'businesses' , $args );
//}
//add_action( 'init', 'create_ctax_business_category', 0 );
//function create_ctax_business_category() {
//	$labels = array(
//		'name' => _x( 'Categories', 'taxonomy general name' ),
//		'singular_name' => _x( 'Category', 'taxonomy singular name' ),
//		'search_items' => __( 'Search Categories' ),
//		'all_items' => __( 'All Categories' ),
//		'parent_item' => __( 'Parent Category' ),
//		'parent_item_colon' => __( 'Parent Category:' ),
//		'edit_item' => __( 'Edit Category' ),
//		'update_item' => __( 'Update Category' ),
//		'add_new_item' => __( 'Add New Category' ),
//		'new_item_name' => __( 'New Category Name' ),
//		'menu_name' => __( 'Categories' ),
//	);
//	register_taxonomy('ctax_business_category','businesses', array(
//		'public' => true,
//		'hierarchical' => true,
//		'labels' => $labels,
//		'show_ui' => true,
//		'show_admin_column' => true,
//		'query_var' => true,
//		'rewrite' => false
//	));
//}

//function custom_taxonomy_flush_rewrite() {
//    global $wp_rewrite;
//    $wp_rewrite->flush_rules();
//}
//add_action('init', 'custom_taxonomy_flush_rewrite');

/***************************************************************
 * Custom Post Type "Activities"
 ***************************************************************/

add_action('init', 'register_activities');

function register_activities(){

    $labels = array(
        'name' => _x('Activities', 'post type general name', 'resort'),
        'singular_name' => _x('Activity', 'post type singular name', 'resort'),
        'menu_name' => _x('Activities', 'admin menu', 'resort'),
        'name_admin_bar' => _x('Activity', 'add new on admin bar', 'resort'),
        'add_new' => _x('Add New', 'book', 'resort'),
        'add_new_item' => __('Add New Activity', 'resort'),
        'new_item' => __('New Activity', 'resort'),
        'edit_item' => __('Edit Activity', 'resort'),
        'view_item' => __('View Activity', 'resort'),
        'all_items' => __('All Activities', 'resort'),
        'search_items' => __('Search Activities', 'resort'),
        'parent_item_colon' => __('Parent Activities:', 'resort'),
        'not_found' => __('No Activities found.', 'resort'),
        'not_found_in_trash' => __('No Activities found in Trash.', 'resort')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'resort'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'activity'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 20,
        'supports' => array('title', 'excerpt', 'thumbnail', 'editor', 'revisions'),
        'menu_icon' => 'dashicons-share-alt', // http://calebserna.com/dashicons-cheatsheet/
    );

    register_post_type('resortactivities', $args);

    $labels = array(
        'name' => _x('Activity Type', 'taxonomy general name', 'resort'),
        'singular_name' => _x('Activity Type', 'taxonomy singular name', 'resort'),
        'search_items' => __('Search Activity Types', 'resort'),
        'all_items' => __('All Activity Types', 'resort'),
        'parent_item' => __('Parent Activity Type', 'resort'),
        'parent_item_colon' => __('Parent Activity Type:', 'resort'),
        'edit_item' => __('Edit Activity Type', 'resort'),
        'update_item' => __('Update Activity Type', 'resort'),
        'add_new_item' => __('Add New Activity Type', 'resort'),
        'new_item_name' => __('New Activity Type', 'resort'),
        'menu_name' => __('Activity Type', 'resort'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'activity_type'),
    );

    register_taxonomy('activity_type', array('resortactivities'), $args);

}


/***************************************************************
 * Custom Post Type "Gallery"
 ***************************************************************/

add_action('init', 'register_gallery');

function register_gallery(){

    $labels = array(
        'name' => _x('Slides', 'post type general name', 'resort'),
        'singular_name' => _x('Slide', 'post type singular name', 'resort'),
        'menu_name' => _x('Slides', 'admin menu', 'resort'),
        'name_admin_bar' => _x('Slides', 'add new on admin bar', 'resort'),
        'add_new' => _x('Add New', 'book', 'resort'),
        'add_new_item' => __('Add New Slide', 'resort'),
        'new_item' => __('New Slide', 'resort'),
        'edit_item' => __('Edit Slide', 'resort'),
        'view_item' => __('View Slide', 'resort'),
        'all_items' => __('All Slides', 'resort'),
        'search_items' => __('Search Slides', 'resort'),
        'parent_item_colon' => __('Parent Slide:', 'resort'),
        'not_found' => __('No Slides found.', 'resort'),
        'not_found_in_trash' => __('No Slides found in Trash.', 'resort')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'resort'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'resortslides'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 20,
        'supports' => array('title', 'thumbnail', 'editor', 'revisions'),
        'menu_icon' => 'dashicons-images-alt2', // http://calebserna.com/dashicons-cheatsheet/
    );

    register_post_type('resortslides', $args);

    $labels = array(
        'name' => _x('Slide_Categories', 'taxonomy general name', 'resort'),
        'singular_name' => _x('Slide Type', 'taxonomy singular name', 'resort'),
        'search_items' => __('Search Slide Types', 'resort'),
        'all_items' => __('All Slide Types', 'resort'),
        'parent_item' => __('Parent Slide Type', 'resort'),
        'parent_item_colon' => __('Parent Slide Type:', 'resort'),
        'edit_item' => __('Edit Slide Type', 'resort'),
        'update_item' => __('Update Slide Type', 'resort'),
        'add_new_item' => __('Add New Slide Type', 'resort'),
        'new_item_name' => __('New Slide Type', 'resort'),
        'menu_name' => __('Slide Type', 'resort'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slide_type'),
    );

    register_taxonomy('slide_type', array('resortslides'), $args);

}

/***************************************************************
 * Custom Post Type "Press"
 ***************************************************************/

add_action('init', 'register_press');

function register_press(){

    $labels = array(
        'name' => _x('Press Articles', 'post type general name', 'resort'),
        'singular_name' => _x('Press Article', 'post type singular name', 'resort'),
        'menu_name' => _x('Press Articles', 'admin menu', 'resort'),
        'name_admin_bar' => _x('Press Articles', 'add new on admin bar', 'resort'),
        'add_new' => _x('Add New', 'book', 'resort'),
        'add_new_item' => __('Add New Press Article', 'resort'),
        'new_item' => __('New Press Article', 'resort'),
        'edit_item' => __('Edit Press Article', 'resort'),
        'view_item' => __('View Press Article', 'resort'),
        'all_items' => __('All Press Articles', 'resort'),
        'search_items' => __('Search Press Articles', 'resort'),
        'parent_item_colon' => __('Parent Press Article:', 'resort'),
        'not_found' => __('No Press Article found.', 'resort'),
        'not_found_in_trash' => __('No Press Article found in Trash.', 'resort')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'resort'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'press'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 20,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'menu_icon' => 'dashicons-welcome-widgets-menus', // http://calebserna.com/dashicons-cheatsheet/
    );

    register_post_type('press_article', $args);

    $labels = array(
        'name' => _x('Press Article Types', 'taxonomy general name', 'resort'),
        'singular_name' => _x('Press Article Type', 'taxonomy singular name', 'resort'),
        'search_items' => __('Search Press Article Types', 'resort'),
        'all_items' => __('All Press Article Types', 'resort'),
        'parent_item' => __('Parent Press Article Type', 'resort'),
        'parent_item_colon' => __('Parent Press Article Type:', 'resort'),
        'edit_item' => __('Edit Press Article Type', 'resort'),
        'update_item' => __('Update Press Article Type', 'resort'),
        'add_new_item' => __('Add New Press Article Type', 'resort'),
        'new_item_name' => __('New Press Article Type', 'resort'),
        'menu_name' => __('Press Article Types', 'resort'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'press_article_type'),
    );

    register_taxonomy('press_article_type', array('press_article'), $args);

}


/***************************************************************
 * Custom post type  "Job Postings"
 ***************************************************************/

add_action('init', 'register_jobpostings');

function register_jobpostings() {

    $labels = array(
        'name' => __('Job Postings'),
        'menu_name' => __('Job Postings'),
        'singular_name' => __('Job Posting'),
        'add_new_item' => __('Add New Job Posting'),
        'edit_item' => __('Edit Job Posting'),
        'new_item' => __('New Job Posting'),
        'view_item' => __('View Job Posting'),
        'search_items' => __('Search Job Postings'),
        'not_found' => __('No Job Postings found'),
        'not_found_in_trash' => __('No Job Postings found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'careers', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-list-view', // http://calebserna.com/dashicons-cheatsheet/
        'has_archive' => true,
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'can_export' => true,
    );

    register_post_type('jobpostings', $args);
}

add_action('init', 'create_jobposting_category', 0);

function create_jobposting_category() {

    $labels = array(
        'name' => _x('Job Categories', 'taxonomy general name'),
        'singular_name' => _x('Job Category', 'taxonomy singular name'),
        'search_items' => __('Search Job Categories'),
        'all_items' => __('All Job Categories'),
        'parent_item' => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item' => __('Edit Category'),
        'update_item' => __('Update Category'),
        'add_new_item' => __('Add New Category'),
        'new_item_name' => __('New Category Name'),
        'menu_name' => __('Categories'),
    );
    register_taxonomy('jobposting_category', 'jobpostings', array(
        'public' => true,
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => false
    ));
}
function remove_row_actions( $actions ) {
    if( get_post_type() === 'jobpostings' )
        unset( $actions['view'] );
    return $actions;
}
add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );

function custom_taxonomy_flush_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}

add_action('init', 'custom_taxonomy_flush_rewrite');


