<?php

/******************************************************************************
 * Setup Functions for a development flag when on Cloudbox or Dev.  Hide in UA / Production.
 ******************************************************************************/

$sbox_ip_pattern = '1.1.1.1';    // Pattern For Development Cloudboxes
$dev_server_ip = '0.0.0.0';      // Dev Server

$current_server_ip = $_SERVER['SERVER_ADDR'];

function is_dev() {
    global $current_server_ip, $dev_server_ip;
    return $current_server_ip == $dev_server_ip;
}

function is_sbox() {
    global $current_server_ip, $sbox_ip_pattern;
    return strpos($current_server_ip, $sbox_ip_pattern) !== false;
}


/**********************************************************************************************
 * Disable WordPress Update Notifications and Plugin Update Notifications on UA and Production
 **********************************************************************************************/

//  If Not on Dev and not Cloudbox, add these filters
if (!is_dev() and !is_sbox()) {

    function remove_core_updates() {
        global $wp_version;
        return (object)array('last_checked' => time(), 'version_checked' => $wp_version,);
    }

    add_filter('pre_site_transient_update_core', 'remove_core_updates');
    add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
    add_filter('pre_site_transient_update_themes', 'remove_core_updates');

}
