<?php

/***************************************************************
 * CUSTOM MCE SETTINGS - Custom Classes for Wordpress WYSIWYG
 ***************************************************************/

/***************************************************************
 * Add basic styles to tinymce
 ***************************************************************/

function formatTinyMCE($in)
{
    $in['content_css'] = "/content/themes/base/css/wysiwyg.css";
    return $in;
}

add_filter('tiny_mce_before_init', 'formatTinyMCE');

/***************************************************************
 * Remove the Stylesheet dropdown
 ***************************************************************/

function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/***************************************************************
 * Additional MCE Formats
 ***************************************************************/

function mce_additional_formats($init_array) {
    $style_formats = array(
        array(
            'title' => 'Headings',
            'items' => array(
                array(
                    'title' => 'Heading 2',
                    'selector' => 'p, span, h2, h3, h4, h5, h6',
                    'classes' => 'h2',
                ),
                array(
                    'title' => 'Heading 3',
                    'selector' => 'p, span, h2, h3, h4, h5, h6',
                    'classes' => 'h3',
                ),
                array(
                    'title' => 'Heading 4',
                    'selector' => 'p, span, h2, h3, h4, h5, h6',
                    'classes' => 'h4',
                ),
                array(
                    'title' => 'Heading 5',
                    'selector' => 'p, span, h2, h3, h4, h5, h6',
                    'classes' => 'h5',
                ),
                array(
                    'title' => 'Heading 6',
                    'selector' => 'p, span, h2, h3, h4, h5, h6',
                    'classes' => 'h6',
                ),
            ),
        ),
        array(
            'title' => 'General Text',
            'items' => array(
                array(
                    'title' => 'Text Colors',
                    'items' => array(
                        array(
                            'title' => 'Default',
                            'selector' => 'p, span, h2, h3, h4, h5, h6',
                            'classes' => 'text-color--default',
                        ),
                        array(
                            'title' => 'Primary',
                            'selector' => 'p, span, h2, h3, h4, h5, h6',
                            'classes' => 'text-color--primary',
                        ),
                        array(
                            'title' => 'Secondary',
                            'selector' => 'p, span, h2, h3, h4, h5, h6',
                            'classes' => 'text-color--secondary',
                        ),
                        array(
                            'title' => 'White',
                            'selector' => 'p, span, h2, h3, h4, h5, h6',
                            'classes' => 'text-color--white',
                        ),
                    ),
                ),
                array(
                    'title' => 'Inline Text Colors',
                    'items' => array(
                        array(
                            'title' => 'Default',
                            'inline' => 'span',
                            'classes' => 'text-color--default',
                        ),
                        array(
                            'title' => 'Primary',
                            'inline' => 'span',
                            'classes' => 'text-color--primary',
                        ),
                        array(
                            'title' => 'Secondary',
                            'inline' => 'span',
                            'classes' => 'text-color--secondary',
                        ),
                        array(
                            'title' => 'White',
                            'inline' => 'span',
                            'classes' => 'text-color--white',
                        ),
                    ),
                ),
            ),
        ),

        array(
            'title' => 'Buttons',
            'items' => array(
                array(
                    'title' => 'Block',
                    'selector' => 'a',
                    'classes' => 'btn btn-primary',
                ),
                array(
                    'title' => 'Text',
                    'selector' => 'a',
                    'classes' => 'txt-btn',
                ),
            ),
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'mce_additional_formats');

/***************************************************************
 * Removing emojis
 ***************************************************************/
function disable_wp_emojicons() {
    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // filter to remove TinyMCE emojis
    add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');

    // Remove the DNS prefetch by returning false on filter emoji_svg_url
    add_filter('emoji_svg_url', '__return_false');
}

add_action('init', 'disable_wp_emojicons');

function disable_emojicons_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}
