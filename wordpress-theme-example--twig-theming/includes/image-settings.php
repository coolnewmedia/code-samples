<?php
/***************************************************************
 * Setup featured images and custom image sizes
 ***************************************************************/
function image_setup() {
    add_theme_support('post-thumbnails');

    set_post_thumbnail_size(250, 200, true);

    //add_image_size( $name, $width, $height, $crop );

    add_image_size('mobile-homepage-hero',       500, 800, true);
    add_image_size('hero-image-front-page',     1920, 1080, true);
    add_image_size('hero-image',                1920, 600, true);
    add_image_size('footer',                    1920, 700, true);
    add_image_size('image-right-left',          1248, 832, true);
    add_image_size('image-right-left-mobile',   624, 416, true);
    add_image_size('image-right-left-overlay',  1632, 1088, true);
    add_image_size('two-images-diagonal',       1050, 700, true);
    add_image_size('single-image-full',         1370, 940, true);
    add_image_size('gallery-image',             560, 281, true);
    add_image_size('post-archive-thumbnail',    444, 480, true);
    add_image_size('activities-listing',        480, 480, true);
    add_image_size('post-listings-modal-lg',    1000, 500, true);
    add_image_size('post-listings-modal-sm',    600, 315, true);
    add_image_size('meeting-archive-thumbnail', 1533, 850, true);
    add_image_size('press-related-posts',       445, 350, true);
    add_image_size('other-rooms-cta',       444, 480, true);


    add_image_size('gallery-slider-image',      1380, 668, true);
    add_image_size('gallery-slider-thumbnail',  162, 135, true);
}

add_action('after_setup_theme', 'image_setup');


/***************************************************************
 * Set default JPG image compression
 ***************************************************************/
function custom_jpg_compression($args) {
    return 100;
}

add_filter('jpeg_quality', 'custom_jpg_compression');
