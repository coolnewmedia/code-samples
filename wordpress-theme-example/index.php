<?php
get_header();

$data = array();
$data['layout_num'] = 0;
// Check if ACF is installed and loop through custom flex layouts
if (function_exists('has_sub_field')):
    while (have_rows('page_layout')) : // Loop through Flexible Content
        the_row();
        // Send $data as variable so we can use each field as a variable immediately.
        $data['row'] = get_row(true);
        $data['layout_num']++;
        ACF_Layout::render(get_row_layout(), $data);
    endwhile;
endif; ?>
    THE INDEX FILE :(
<?php get_footer();