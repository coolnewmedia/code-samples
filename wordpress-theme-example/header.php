<!doctype html>
<!--[if IE 9 ]>
<html lang="en" class="ie9 lt-ie10 lt-ie11"> <![endif]-->
<!--[if IE 10 ]>
<html lang="en" class="ie10 lt-ie11"> <![endif]-->
<!--[if gt IE 10 ]>
<html lang="en" class="ie11"> <![endif]-->
<!--[if !(IE)]><!-->
<html id="front" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php wp_title('|', true, 'right'); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <?php wp_head(); ?>

    <?php /*Fancybox   REF: http://fancyapps.com/fancybox/3/ --used for Video Gallery */ ?>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>

    <?php $aboveFoldCSS = dirname(__FILE__) . '/css/above-fold.css'; ?>
    
    <?php if (file_exists($aboveFoldCSS)) : ?>
        <?php $aboveFoldCSSContents = file_get_contents($aboveFoldCSS); ?>
        <style media="screen" type="text/css"><?php echo $aboveFoldCSSContents; ?></style>
    <?php endif; ?>

    <?php $jquery = '/js/jquery.js'; ?>
    <?php if (file_exists(dirname(__FILE__) . $jquery)) : ?>
        <?php $jqueryFoldJSMtime = filemtime(dirname(__FILE__) . $jquery); ?>
        <script type="text/javascript"
                src="<?php echo get_template_directory_uri(); ?><?php echo $jquery; ?>?v=<?php echo $jqueryFoldJSMtime; ?>"></script>
    <?php endif; ?>

    <?php $aboveFoldJS = '/js/above-fold.js'; ?>
    <?php if (file_exists(dirname(__FILE__) . $aboveFoldJS)) : ?>
        <?php $aboveFoldJSMtime = filemtime(dirname(__FILE__) . $aboveFoldJS); ?>
        <script type="text/javascript"
                src="<?php echo get_template_directory_uri(); ?><?php echo $aboveFoldJS; ?>?v=<?php echo $aboveFoldJSMtime; ?>"
                defer></script>
    <?php endif; ?>

</head>
<body <?php body_class(); ?>>

<div class="site-wrap">

    <header>
        <?php include(locate_template('template-parts/header-primary-menu.php', false, false)); ?>
    </header>