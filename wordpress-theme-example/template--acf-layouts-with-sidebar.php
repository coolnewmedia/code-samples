<?php
/* Template Name: ACF Layouts with Sidebar */
get_header();

?>

    <div class="basic-page-with-sidebar">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png"
             alt="ripple background"/>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9 main-content">

                    <?php
                    $data = array();
                    $data['layout_num'] = 0;
                    // Check if ACF is installed and loop through custom flex layouts
                    if (function_exists('has_sub_field')):
                        while (have_rows('page_layouts')) : // Loop through Flexible Content
                            the_row();
                            // Send $data as variable so we can use each field as a variable immediately.
                            $data['row'] = get_row(true);
                            $data['layout_num']++;
                            ACF_Layout::render(get_row_layout(), $data);
                        endwhile;
                    endif;
                    ?>

                </div>
                <a class="mobile-sidebar-toggle btn" role="button" data-toggle="collapse" href="#sidebar" role="button" aria-expanded="false" aria-controls="sidebar">
                    Navigation
                </a>
                <div class="col-sm-12 col-md-4 col-lg-3 sidebar sidebar-desktop">
                    <?php echo wpse_get_ancestor_tree(); ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 sidebar sidebar-mobile collapse" id="sidebar">
                    <?php echo wpse_get_ancestor_tree(); ?>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();