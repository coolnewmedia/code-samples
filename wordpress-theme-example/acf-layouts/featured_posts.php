<?php

$heading = get_field('heading');
$content_type = get_sub_field('content_type');
$post_ids = get_sub_field('posts', false, false);
$blog_ids = get_sub_field('blog_posts', false, false);
$show_excerpts = get_sub_field('show_excerpts');

if ($content_type == 'other') :
    $query = new WP_Query(array(
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'orderby' => 'post__in',
        'post__in' => $post_ids,
        'post_type' => 'any'
    ));
else:
    $query = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'orderby' => 'post__in',
        'post__in' => $blog_ids,
    ));
endif;

$default_post_image = get_field('default_post_preview_image', 'option');
$fly_default = fly_get_attachment_image_src($default_post_image['id'], 'listing_thumbnail', true);

?>

<section class="featured-posts acf-layout <?php if ($css_class) : echo $css_class; endif; ?>">
    <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged--mountain.png"
         alt="ripple background"/>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="featured-posts__intro">
                    <?php if ($heading) : ?>
                        <h2><?php echo $heading; ?></h2>
                    <?php elseif ($content_type == 'blog' && !$heading) : ?>
                        <h2 class="blog-heading"><span>Stories from our</span> <i>Adventure Log</i></h2>
                    <?php endif; ?>
                    <?php if ($content_type = 'blog') : ?>
                        <a href="/blog/">
                            View All Stories
                        </a>
                    <?php endif; ?>
                </div>
                <?php if ($query->have_posts()) : ?>
                    <?php $count = $query->post_count; ?>
                    <div class="featured-posts__container">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ($query->have_posts()) :
                                    $query->the_post();
                                    $content = get_field('content');
                                    $details = get_field('details');
                                    $read_more_button_text = get_field('read_more_button_text');

                                    $fly_image = fly_get_attachment_image_src(get_post_thumbnail_id(), 'listing_thumbnail', true);
                                    $fly_image_alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);

                                ?>
                                    <div class="post swiper-slide post__count-<?php echo $count; ?>">
                                        <div class="post__inner">
                                            <div class="post__image">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if (has_post_thumbnail()) : ?>
                                                        <img class="lazyload"
                                                             data-src="<?php echo $fly_image['src']; ?>"
                                                             src="/content/themes/base/img/placeholder.gif"
                                                             alt="<?php echo $fly_image_alt; ?>"/>
                                                    <?php elseif ($default_post_image) : ?>
                                                        <img class="lazyload"
                                                             data-src="<?php echo $default_post_image['url']; ?>"
                                                             src="/content/themes/base/img/placeholder.gif"
                                                             alt="<?php echo $default_post_image['alt']; ?>"/>
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="post__content">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <?php if ($show_excerpts) : ?>
                                                    <?php if (has_excerpt()) :
                                                        the_excerpt();
                                                    elseif ($content) :
                                                        echo wp_trim_words($content, 20, '...');
                                                    elseif ($details) :
                                                        echo wp_trim_words($details, 20, '...');
                                                    else:
                                                        echo wp_trim_words(get_the_content(), 20, '...');
                                                    endif; ?>
                                                <?php endif; ?>
                                                <a class="btn" href="<?php the_permalink(); ?>">
                                                    <?php if ($read_more_button_text) : ?>
                                                        <?php echo($read_more_button_text); ?>
                                                    <?php else: ?>
                                                        Read More
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="swiper-buttons swiper__count-<?php echo $count; ?>">
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>