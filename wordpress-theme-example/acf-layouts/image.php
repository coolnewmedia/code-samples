<?php
/**
 * ACF Layout: Image
 *
 * Kint dump all the fields
 * ------------------------
 * $fields = get_fields();
 * d($fields)
 *
 */

$fly_image_mobile = fly_get_attachment_image_src($image['id'], 'post_image_mobile', true);
$fly_image_tablet = fly_get_attachment_image_src($image['id'], 'post_image_tablet', true);
$fly_image_desktop = fly_get_attachment_image_src($image['id'], 'post_image_desktop', true);

// We are going to use the Figure tag with FigCaption to make this image fully accessible.
?>

<section class="acf-layout acf-layout__image">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10">
                <figure>
                    <picture>
                        <source srcset="<?php echo $fly_image_desktop['src']; ?>"
                                media="(min-width:1440px)">
                        <source srcset="<?php echo $fly_image_tablet['src']; ?>"
                                media="(min-width:768px)">
                        <img src="<?php echo $fly_image_mobile['src']; ?>" alt="<?php echo $image['alt']; ?>">
                    </picture>
                    <figcaption><?php echo $caption; ?></figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>