<?php

$vacation_guide_pdf = get_field('vacation_guide_pdf', 'options');
$vacation_guide_image = get_field('vacation_guide_image', 'options');
$vacation_guide_heading = get_field('vacation_guide_heading', 'options');

$fly_image_mobile = fly_get_attachment_image_src($background_image['id'], 'hero_mobile', true);
$fly_image_tablet = fly_get_attachment_image_src($background_image['id'], 'hero_tablet', true);
$fly_image_desktop = fly_get_attachment_image_src($background_image['id'], 'hero_desktop', true);

switch ($content_width) {
    case "narrow" :
        $container_classes = "col-sm-12 col-lg-10 col-xl-8";
        $content_classes ="col-12 col-md-8 col-lg-9";
        $cta_classes = "col-12 col-md-4 col-lg-3";
        break;
    case "default" :
        $container_classes = "col-sm-12 col-lg-10";
        $content_classes ="col-12 col-md-8";
        $cta_classes = "col-12 col-md-4";
        break;
}

?>
<section class="vacation-guide-cta acf-layout
    <?php
if ($css_class) : echo $css_class; endif;
if ($background_image && $texture != 'tan') : echo ' has-bg-img'; endif;
if ($texture == 'tan') :
    echo ' textured--tan';
elseif ($texture == 'white') :
    echo ' textured--white';
endif;
?>
">
    <?php if ($texture == 'tan') : ?>
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged--mountain-2.png"
             alt="ripple background"/>
    <?php elseif ($texture == 'white') : ?>
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png"
             alt="ripple background"/>
    <?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="<?php echo $container_classes;?> vacation-guide-cta__container">
                <div class="row">
                    <div class="<?php echo $content_classes;?>">
                        <?php if ($heading) : ?>
                            <?php if ($heading) : ?>
                                <?php echo '<' . $heading_level . '>'; ?>
                                <?php echo $heading; ?>
                                <?php echo '</' . $heading_level . '>'; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php echo $content; ?>
                    </div>
                    <div class="<?php echo $cta_classes;?> vacation-guide">
                        <?php if ($vacation_guide_pdf) : ?>
                            <?php if ($vacation_guide_image) : ?>
                                <div class="vacation-guide__image">
                                    <img class="lazyload"
                                         data-src="<?php echo $vacation_guide_image['url']; ?>"
                                         src="/content/themes/base/img/placeholder.gif"
                                         alt="<?php echo $vacation_guide_image['alt']; ?>"/>
                                </div>
                            <?php endif; ?>
                            <div class="vacation-guide__content">
                                <?php if ($vacation_guide_heading) : ?>
                                    <h2 class="h3"><?php echo $vacation_guide_text; ?></h2>
                                <?php endif; ?>
                                <p role="button" class="download" data-toggle="modal"
                                   data-target="#vacationGuideDownload">
                                    <?php echo $vacation_guide_modal_link_text; ?>
                                </p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($background_image && $texture != 'tan') : ?>

        <div class="background">
            <picture>
                <source data-srcset="<?php echo $fly_image_desktop['src']; ?>"
                        media="(min-width:768px)">
                <img class="background lazyload"
                     data-src="<?php echo $fly_image_tablet['src']; ?>"
                     src="/content/themes/base/img/placeholder.gif"
                     alt="<?php echo $background_image['alt']; ?>">
            </picture>
        </div>

    <?php endif; ?>



</section>