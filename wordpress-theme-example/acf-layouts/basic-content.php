<?php
/**
 * Basic Content ACF Layout
 *
 * Kint dump all the fields
 * ------------------------
 * $fields = get_fields();
 * d($fields)
 *
 */


//Check Heading Level as it was added after content was already created
if (!isset($heading_level)) :
    $heading_level = 'h2';
endif;

// is the Heading Centered.
$centeredtext = $center_heading_text ? "centered" : "";

// Get the current template in use
$current_template = get_page_template_slug();

// Set the default column classes
$column_classes = 'col-sm-12 col-lg-10 col-xl-8';
$padding = '';

// If the template is a page with sidebar, change the column classes
if ($current_template == 'template--acf-layouts-with-sidebar.php') {
    $column_classes = 'col-12';
};

?>

<section class="basic-content acf-layout
    <?php if ($css_class) : echo $css_class; endif;
if ($texture == 'tan') :
    echo ' textured--tan';
elseif ($texture == 'white') :
    echo ' textured--white';
endif; ?>
">

    <?php if ($texture == 'tan') : ?>
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged-mountain.png"
             alt="ripple background"/>
    <?php elseif ($texture == 'white') : ?>
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png"
             alt="ripple background"/>
    <?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="<?php echo $column_classes;?>">

                <?php if ($heading) : ?>
                    <?php echo '<' . $heading_level . ' class="' . $centeredtext . '">' ?>
                    <?php echo $heading; ?>
                    <?php echo '</' . $heading_level . '>'; ?>
                <?php endif; ?>

                <?php echo $content; ?>

                <?php get_template_part('template-parts/buttons'); ?>

            </div>
        </div>
    </div>

</section>