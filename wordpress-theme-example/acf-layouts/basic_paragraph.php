<?php
/**
 * ACF Layout: Basic Paragraph
 *
 * Kint dump all the fields
 * ------------------------
 * $fields = get_fields();
 * d($fields)
 *
 */
?>

<section class="acf-layout acf-layout__basic-paragraph">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-10 col-xl-8">
                <?php echo $basic_paragraph; ?>
            </div>
        </div>
    </div>
</section>