<?php
$count = 0;
?>
<section class="two-tiles acf-layout">
    <div class="row">
        <?php if (have_rows('tiles')) : while (have_rows('tiles')) : the_row(); ?>
            <div class="col-sm-12 col-md-6 tile tile__<?php echo $count; ?>">
                <?php

                $background_image = get_sub_field('background_image');

                $fly_image_mobile = fly_get_attachment_image_src($background_image['id'], 'two_tiles_mobile', true);
                $fly_image_tablet = fly_get_attachment_image_src($background_image['id'], 'two_tiles_tablet', true);
                $fly_image_desktop = fly_get_attachment_image_src($background_image['id'], 'two_tiles_desktop', true);

                $heading = get_sub_field('heading');

                ?>

                <div class="tile__content">
                    <?php if ($heading) : ?>
                        <h3><?php echo $heading; ?></h3>
                    <?php endif; ?>
                    <?php get_template_part('template-parts/buttons'); ?>
                </div>

                <div class="tile__image">
                    <picture>
                        <source data-srcset="<?php echo $fly_image_desktop['src']; ?>"
                                media="(min-width:1440px)">
                        <source data-srcset="<?php echo $fly_image_tablet['src']; ?>"
                                media="(min-width:768px)">
                        <img data-src="<?php echo $fly_image_mobile['src']; ?>"
                             src="/content/themes/base/img/placeholder.gif"
                             class="lazyload"
                             alt="<?php echo $background_image['alt']; ?>"/>
                    </picture>
                </div>
            </div>
            <?php $count++; ?>
        <?php endwhile; endif; ?>
    </div>
</section>