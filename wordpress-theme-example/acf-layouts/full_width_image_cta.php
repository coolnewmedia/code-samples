<?php
/**
 * ACF Layout: Full Width CTA with Background Image
 *
 * Kint dump all the fields
 * ------------------------
 * $fields = get_fields();
 * d($fields)
 *
 */

// $image is required.
$fly_image_tablet = fly_get_attachment_image_src($image['id'], 'hero_tablet', true);
$fly_image_desktop = fly_get_attachment_image_src($image['id'], 'hero_desktop', true);

// Check if we have a logo lockup
if ($logo_lockup) :
    $fly_logo_lockup = fly_get_attachment_image_src($logo_lockup['id'], 'logo_lockup', true);
endif;

?>
<section class="full-width-image-cta acf-layout <?php if ($css_class) : echo $css_class; endif; ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php if ($logo_lockup) : ?>
                    <img class="logo-lockup lazyload"
                         data-src="<?php echo $fly_logo_lockup['src']; ?>"
                         src="/content/themes/base/img/placeholder.gif"
                         alt="<?php echo $logo_lockup['alt']; ?>">
                <?php endif; ?>
                <div class="full-width-image-cta__content">
                    <?php if ($heading) : ?>
                        <h3><?php echo $heading; ?></h3>
                    <?php endif; ?>
                    <?php echo $content; ?>
                    <?php get_template_part('template-parts/buttons'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="cover-picture">
        <picture>
            <source data-srcset="<?php echo $fly_image_desktop['src']; ?>"
                    media="(min-width:768px)">
            <img class="background lazyload"
                 data-src="<?php echo $fly_image_tablet['src']; ?>"
                 src="/content/themes/base/img/placeholder.gif"
                 alt="<?php echo $image['alt']; ?>">
        </picture>
    </div>
</section>