<?php

/**
 * Image/Video Gallery - ACF Layout
 *
 * Kint dump all the fields
 * ------------------------
$fields = get_fields();
d($fields)
 *
 */

?>
<section class="gallery acf-layout <?php echo $gallery_content; ?>">
    <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged--mountain-2.png" alt="ripple background"/>
    <div class="container">
        <div class="row">

            <?php if (have_rows('videos') && $gallery_content == 'videos') : ?>
                <?php while (have_rows('videos')) : the_row(); ?>
                    <div class="gallery-item video-item">
                        <?php
                        $video_url = get_sub_field('video_url');
                        $thumbnail = get_sub_field('thumbnail_image');
                        $thumbnail_image = fly_get_attachment_image($thumbnail['id'], 'listing_thumbnail');
                        $heading = get_sub_field('heading');
                        $content = get_sub_field('content');
                        ?>

                        <div class="gallery-item__inner">
                            <div class="gallery-item__image">
                                <a data-fancybox="gallery" href="<?php echo $video_url; ?>">
                                    <?php echo $thumbnail_image; ?>
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 60 60" xml:space="preserve">
                                        <path d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M45.563,30.826l-22,15
	C23.394,45.941,23.197,46,23,46c-0.16,0-0.321-0.038-0.467-0.116C22.205,45.711,22,45.371,22,45V15c0-0.371,0.205-0.711,0.533-0.884
	c0.328-0.174,0.724-0.15,1.031,0.058l22,15C45.836,29.36,46,29.669,46,30S45.836,30.64,45.563,30.826z"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <?php if ($heading || $content) : ?>
                            <div class="gallery-item__content">
                                <?php if ($heading) : ?>
                                    <h4><?php echo $heading; ?></h4>
                                <?php endif; ?>

                                <?php if ($content) : ?>
                                <?php echo $content; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                <?php endwhile; ?>
            <?php elseif ($images) : ?>
                <?php foreach ($images as $image): ?>
                    <div class="gallery-item">
                        <div class="gallery-item__inner">
                            <div class="gallery-item__image">
                                <a data-fancybox="gallery" href="<?php echo $image['url']; ?>">
                                    <?php echo fly_get_attachment_image($image['id'], 'listing_thumbnail'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</section>