<?php
$count = count(get_sub_field('columns'));
?>
<section class="content-columns acf-layout has-<?php echo $count; ?>-columns">
    <div class="container">
        <div class="row">
            <?php if (have_rows('columns')) : while (have_rows('columns')) : the_row(); ?>
                <div class="col column">
                    <?php
                    $image = get_sub_field('image');
                    $heading = get_sub_field('heading');
                    $content = get_sub_field('content');

                    $fly_image = fly_get_attachment_image_src($image['id'], 'two_col_desktop', true);
                    ?>
                    <?php if ($image) : ?>
                        <div class="column__image">
                            <img class="lazyload" data-src="<?php echo $fly_image['src']; ?>"
                                 src="/content/themes/base/img/placeholder.gif"
                                 alt="<?php echo $image['alt']; ?>"/>
                        </div>
                    <?php endif; ?>
                    <div class="column__content">
                        <?php if ($heading) : ?>
                            <h3><?php echo $heading; ?></h3>
                        <?php endif; ?>
                        <div class="text">
                            <?php echo $content; ?>
                        </div>
                        <?php get_template_part('template-parts/buttons'); ?>
                    </div>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>