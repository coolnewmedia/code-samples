<?php
/**
 * Single Video - ACF Layout
 */
?>
<section class="single-video acf-layout">
    <div class="container">
        <?php echo $video_url; ?>
    </div>
</section>