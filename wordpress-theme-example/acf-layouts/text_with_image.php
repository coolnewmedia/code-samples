<?php
/**
 * ACF Layout: Text with Image
 *
 * Kint dump all the fields
 * ------------------------
 * $fields = get_fields();
 * d($fields)
 *
 */

$fly_image_mobile = fly_get_attachment_image_src($image['id'], 'post_text_with_image_mobile', true);
$fly_image_tablet = fly_get_attachment_image_src($image['id'], 'post_text_with_image_tablet', true);
$fly_image_desktop = fly_get_attachment_image_src($image['id'], 'post_text_with_image_desktop', true);


$image_col = "col-12 col-md-4 col-lg-3 col-xl-3 image offset-lg-1 pl-lg-0";
if ($image_position == "left") :
    $image_col = "col-12 col-md-4 col-lg-3 col-xl-3 image pr-lg-0";
endif;

?>

<section class="acf-layout acf-layout__text-with-image">
    <div class="container">
        <div class="row image-<?php echo $image_position; ?>">
            <?php if ($image_position == "right") : ?>
                <div class="col-12 col-md-7 col-lg-6 col-xl-5 offset-xl-1 pr-lg-0">
                    <?php echo $text; ?>
                </div>
            <?php endif; ?>
            <div class="<?php echo $image_col; ?>">
                <figure>
                    <picture>
                        <source data-srcset="<?php echo $fly_image_desktop['src']; ?>"
                                media="(min-width:1440px)">
                        <source data-srcset="<?php echo $fly_image_tablet['src']; ?>"
                                media="(min-width:768px)">
                        <img class="lazyload"
                             data-src="<?php echo $fly_image_mobile['src']; ?>"
                             src="/content/themes/base/img/placeholder.gif"
                             alt="<?php echo $image['alt']; ?>">
                    </picture>
                    <figcaption><?php echo $caption; ?></figcaption>
                </figure>
            </div>
            <?php if ($image_position == "left") : ?>
                <div class="col-12 col-md-7 col-lg-6 col-xl-5 offset-lg-1 pl-lg-0">
                    <?php echo $text; ?>
                </div>
                <div class="col-xl-1 order-last"></div>
            <?php endif; ?>
        </div>
    </div>
</section>