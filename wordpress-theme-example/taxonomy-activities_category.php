<?php

get_header();

$term = get_queried_object();
$term_id = $term->term_id;
$taxonomy_name = $term->taxonomy;

$hero_image = get_field('hero_image', $term);
$default_single_post_hero = get_field('default_single_post_hero_image', 'option');

// Convert Taxonomy Hero to responsive sizes
$fly_image_mobile = fly_get_attachment_image_src($hero_image['id'], 'hero_large_mobile', true);
$fly_image_tablet = fly_get_attachment_image_src($hero_image['id'], 'hero_large_tablet', true);
$fly_image_desktop = fly_get_attachment_image_src($hero_image['id'], 'hero_large_desktop', true);

$display_title = get_field('display_title', $term);
$content = get_field('content', $term);

$default_post_image = get_field('default_post_preview_image', 'option');

/* We call is categories here because we are using Activity Categories taxonomy.  Should be terms by we keep it consistant with the see do page.   */
$categories = get_terms($term->taxonomy, array(
    'child_of' => $term->term_id,
    'hide_empty' => false,
));

$has_term_children = get_term_children($term_id, $taxonomy_name);

?>
    <section id="hero" class="hero-inner hero-large">
        <div class="item">
            <?php if ($hero_image) : ?>
                <picture>
                    <source srcset="<?php echo $fly_image_desktop['src']; ?>" media="(min-width:1440px)">
                    <source srcset="<?php echo $fly_image_tablet['src']; ?>" media="(min-width:768px)">
                    <img class="background" src="<?php echo $fly_image_mobile['src']; ?>"
                         alt="<?php echo $hero_image['alt']; ?>">
                </picture>
            <?php elseif ($default_single_post_hero) : ?>
                <img src="<?php echo $default_single_post_hero['url']; ?>"
                     alt="<?php echo $default_single_post_hero['alt']; ?>"/>
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg"
                     alt="Scenic lake view of the Country with rolling hills and mountains in the distance"/>
            <?php endif; ?>
        </div>
    </section>


    <div class="activities-archive">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png"
             alt="Ripple background"/>
        <div class="container activities-list">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9 main-content">
                    <h1>
                        <?php if ($display_title) : ?>
                            <?php echo $display_title; ?>
                        <?php else: ?>
                            <?php single_term_title(); ?>
                        <?php endif; ?>
                    </h1>
                    <div class="activity-content">
                        <?php echo $content; ?>
                    </div>
                    <?php
                    /* If the term is a parent term and has child terms, display the child terms */
                    ?>

                    <?php if (!empty($has_term_children)) : ?>
                        <div class="activities-list__container">
                            <?php foreach ($categories as $category) : ?>
                                <div class="activity">
                                    <div class="activity__inner">
                                        <?php $thumbnail = get_field('thumbnail', $category); ?>
                                        <div class="activity__image">
                                            <a href="<?php echo $category->slug; ?>/"
                                               title="<?php echo $category->name; ?>">
                                                <?php if ($thumbnail) : ?>
                                                    <?php echo fly_get_attachment_image($thumbnail['id'], 'listing_thumbnail'); ?>
                                                <?php elseif ($default_post_image) : ?>
                                                    <?php echo fly_get_attachment_image($default_post_image['id'], 'listing_thumbnail'); ?>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="activity__content">
                                            <div class="activity__info">
                                                <h3><a href="<?php echo $category->slug; ?>/"
                                                       title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                                                </h3>
                                                <?php if ($category->description) :
                                                    echo $category->description;
                                                endif; ?>
                                            </div>
                                            <a class="details btn" href="<?php echo $category->slug; ?>/"
                                               title="<?php echo $category->name; ?>">
                                                Learn More
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>


                    <?php

                    /*
                     * Else, if the term does not have any children, show the posts under this term
                     * */

                    elseif (have_posts()) : ?>

                        <div class="activities-list__container">
                            <?php while (have_posts()) : the_post();
                                $website = get_field('website');
                                $phone = get_field('phone_number');

                                $fly_image = fly_get_attachment_image_src(get_post_thumbnail_id(), 'listing_thumbnail', true);
                                $fly_image_alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
                                ?>
                                <div class="activity">
                                    <div class="activity__inner">
                                        <div class="activity__image">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php if (has_post_thumbnail()) : ?>
                                                    <img class="lazyload"
                                                         data-src="<?php echo $fly_image['src']; ?>"
                                                         src="/content/themes/base/img/placeholder.gif"
                                                         alt="<?php echo $fly_image_alt; ?>"/>
                                                <?php elseif ($default_post_image) : ?>
                                                    <img class="lazyload"
                                                         data-src="<?php echo $default_post_image['url']; ?>"
                                                         src="/content/themes/base/img/placeholder.gif"
                                                         alt="<?php echo $default_post_image['alt']; ?>"/>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="activity__content">

                                            <div class="activity__info">
                                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                            </div>

                                            <div class="activity__buttons">
                                                <a class="details btn" href="<?php the_permalink(); ?>">
                                                    View Details
                                                </a>
                                                <?php if ($website) : ?>
                                                    <a class="website btn" href="<?php echo $website; ?>" target="_blank">Website</a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    <?php else: ?>
                        <?php /* Do nothing */ ?>
                    <?php endif; ?>


                </div>
                <button class="mobile-sidebar-toggle btn"
                        type="button"
                        data-toggle="collapse"
                        data-target="#sidebar"
                        aria-expanded="false"
                        aria-controls="sidebar">
                    Navigation
                </button>
                <div class="col-sm-12 col-md-4 col-lg-3 activities-category-list sidebar-desktop">
                    <h3><?php _e('See & Do'); ?></h3>
                    <ul>
                        <?php wp_list_categories(array(
                            'hierarchical' => true,
                            'taxonomy' => 'activities_category',
                            'title_li' => '',
                            'hide_empty' => false
                        )); ?>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 activities-category-list sidebar-mobile collapse" id="sidebar">
                    <h3>What to Do</h3>
                    <ul>
                        <?php wp_list_categories(array(
                            'hierarchical' => true,
                            'taxonomy' => 'activities_category',
                            'title_li' => '',
                            'hide_empty' => false
                        )); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


<?php

include(locate_template('acf-layouts/offers.php', false, false));

get_footer();