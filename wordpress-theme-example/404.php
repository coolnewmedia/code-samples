<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 */


$title_404 = get_field('404_title', 'option');
$subtitle_404 = get_field('404_subtitle', 'option');
$body_404 = get_field('404_body', 'option');

get_header();

?>

    <div id="hero" class="hero-short">
        <div class="item">
            <?php if ($hero_image) : ?>
                <?php $image_desktop = fly_get_attachment_image_src($hero_image['id'], 'hero_desktop'); ?>
                <?php $image_tablet = fly_get_attachment_image_src($hero_image['id'], 'hero_tablet'); ?>
                <?php $image_mobile = fly_get_attachment_image_src($hero_image['id'], 'hero_mobile'); ?>

                <picture>
                    <source data-srcset="<?php echo $image_desktop['src']; ?>" media="(min-width: 1440px)"
                            srcset="<?php echo $image_desktop['src']; ?>">
                    <source data-srcset="<?php echo $image_tablet['src']; ?>" media="(min-width: 768px)"
                            srcset="<?php echo $image_tablet['src']; ?>">
                    <source data-srcset="<?php echo $image_mobile['src']; ?>" media="(min-width: 0px)"
                            srcset="<?php echo $image_mobile['src']; ?>">
                    <img class="lazyload" data-src="<?php echo $image_desktop['src']; ?>"
                         alt="<?php echo $hero_image['alt']; ?>" src="<?php echo $image_desktop['src']; ?>">
                </picture>
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg" alt="Scenic lake view of the Country with rolling hills and mountains in the distance"/>
            <?php endif; ?>
        </div>
    </div>

    <section class="page-not-found basic-content acf-layout textured--white">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png" alt="ripple background" />
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-10 col-xl-8">

                    <?php if ($title_404) : ?>
                        <h1 class="page-title"><?php echo $title_404; ?></h1>
                    <?php endif; ?>

                    <?php if ($subtitle_404) : ?>
                        <h3><?php echo $subtitle_404; ?></h3>
                    <?php endif; ?>

                    <?php echo $body_404; ?>

                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>