<?php

/***************************************************************
 * Gravity forms 'hack' to load JS in footer
 ***************************************************************/

function wrap_gform_cdata_open($content = '') {
    $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
    return $content;
}

function wrap_gform_cdata_close($content = '') {
    $content = ' }, false );';
    return $content;
}

add_filter('gform_cdata_open', 'wrap_gform_cdata_open');

add_filter('gform_cdata_close', 'wrap_gform_cdata_close');


/***************************************************************
 * Disable tab index in gravity forms
 ***************************************************************/

add_filter('gform_tabindex', '__return_false');


/**************************************************************************
 * Replace the Gravity Forms Spinner with transparent gif (base 64 encoded)
 **************************************************************************/
function gf_spinner_replace($image_src, $form) {
    return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'; // relative to you theme images folder
}

add_filter('gform_ajax_spinner_url', 'gf_spinner_replace', 10, 2);


/***************************************************************
* Initiative the Vacation Guide Download open
 *****************************************************************/

add_action( 'gform_after_submission_5', 'vacation_guide_download', 10, 2 );

function vacation_guide_download() {
    $vacation_guide = get_field('vacation_guide_pdf', 'options');
    $url = $vacation_guide['url'];
    ?>
    <script>
        function startDownload()         {
            var url="<?php echo $url;?>";
            window.open(url, 'Download');
        }

        setTimeout('startDownload()', 3000);

    </script>

<?php }

/**************************************************************
 * Brute Force GForm Submit Redirect on Form_3 : Request Info
 ***************************************************************/

add_action( 'gform_after_submission_3', 'redirect_to_thank_you_URL', 10, 2 );

function redirect_to_thank_you_URL() { ?>
    <script>
        window.top.location.href = "/thank-you/";
    </script>
<?php }