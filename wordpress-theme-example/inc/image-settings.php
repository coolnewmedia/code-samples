<?php

/***************************************************************
 * Setup featured images and custom image sizes
 ***************************************************************/

function image_setup() {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(250, 200, true);

    //add_image_size( $name, $width, $height, $crop );

    // Use Fly Dynamic Image sizes BELOW ....
}

add_action('after_setup_theme', 'image_setup');


/***************************************************************
 * Set default JPG image compression
 ***************************************************************/

function custom_jpg_compression($args) {
    return 100;
}

add_filter('jpeg_quality', 'custom_jpg_compression');

/***************************************************************
 * Fly Dynamic Image Sizes
 ***************************************************************/

if (function_exists('fly_add_image_size')) {

    fly_add_image_size('home_page_square', 500, 500, true);
    fly_add_image_size('home_page_square_2x', 1000, 1000, true);

    fly_add_image_size('cropped_top_left', 1000, 1000, array('left', 'top'));

    fly_add_image_size('hero_mobile', 750, 400, false);
    fly_add_image_size('hero_tablet', 1200, 600, false);
    fly_add_image_size('hero_desktop', 1920, 750, false);

    fly_add_image_size('hero_large_mobile', 600, 400, false);
    fly_add_image_size('hero_large_tablet', 1200, 600, false);
    fly_add_image_size('hero_large_desktop', 1920, 800, false);

    fly_add_image_size('listing_thumbnail', 440, 415, true);

    fly_add_image_size('post_image_mobile', 573, 324, true);
    fly_add_image_size('post_image_tablet', 768, 432, true);
    fly_add_image_size('post_image_desktop', 1440, 810, true);

    fly_add_image_size('post_text_with_image_mobile', 573, 324, true);
    fly_add_image_size('post_text_with_image_tablet', 250, 400, true);
    fly_add_image_size('post_text_with_image_desktop', 500, 800, true);

    fly_add_image_size('two_tiles_mobile', 720, 400, true);
    fly_add_image_size('two_tiles_tablet', 600, 600, true);
    fly_add_image_size('two_tiles_desktop', 975, 800, true);

    fly_add_image_size('two_col_desktop', 670, 400, true);

    fly_add_image_size('logo_lockup', 648, 648, false);


    /*
     * REF:  https://wordpress.org/plugins/fly-dynamic-image-resizer/
     * DOCS: https://github.com/junaidbhura/fly-dynamic-image-resizer/wiki
     *
     * USAGE Examples
     * <?php echo fly_get_attachment_image( get_post_thumbnail_id(), 'home_page_square' ); ?>
     * <?php $image = fly_get_attachment_image_src( get_post_thumbnail_id(), 'home_page_square' ); echo '<img src="' . $image['src'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" />'; ?>
     * <?php echo fly_get_attachment_image( get_post_thumbnail_id(), 'cropped_top_left' ); ?>
     * <?php echo fly_get_attachment_image( get_post_thumbnail_id(), array( 500, 500 ), true ); ?>
     * <?php $image = fly_get_attachment_image_src( get_post_thumbnail_id(), 'home_page_square', array( 500, 500 ), true ); echo '<img src="' . $image['src'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '" />'; ?>
     * <?php echo fly_get_attachment_image( get_post_thumbnail_id(), array( 500, 500 ), array( 'right', 'bottom' ) ); ?>
     */

}