<?php

/***************************************************************
 * Offers custom post type
 ***************************************************************/
add_action('init', 'register_offers');

function register_offers() {
    $labels = array(
        'name' => __('Offers'),
        'menu_name' => __('Offers'),
        'singular_name' => __('Offer'),
        'add_new_item' => __('Add New Offer'),
        'edit_item' => __('Edit Offer'),
        'new_item' => __('New Offer'),
        'view_item' => __('View Offer'),
        'search_items' => __('Search Offers'),
        'not_found' => __('No Offers found'),
        'not_found_in_trash' => __('No Offers found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'offers', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-tag', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
    );
    register_post_type('offers', $args);
}

add_action('init', 'create_offers_category', 0);

function create_offers_category() {
    $labels = array(
        'name' => _x('Offer Categories', 'taxonomy general name'),
        'singular_name' => _x('Offer Category', 'taxonomy singular name'),
        'search_items' => __('Search Offer Categories'),
        'all_items' => __('All Offer Categories'),
        'parent_item' => __('Parent Offer Category'),
        'parent_item_colon' => __('Parent Offer Category:'),
        'edit_item' => __('Edit Offer Category'),
        'update_item' => __('Update Offer Category'),
        'add_new_item' => __('Add New Offer Category'),
        'new_item_name' => __('New Offer Category Name'),
        'menu_name' => __('Offer Categories'),
    );
    register_taxonomy('offers_category', 'offers', array(
        'public' => true,
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
    ));
}


/***************************************************************
 * Activities custom post type
 ***************************************************************/
add_action('init', 'register_activities');

function register_activities() {
    $labels = array(
        'name' => __('Activities'),
        'menu_name' => __('Activities'),
        'singular_name' => __('Activity'),
        'add_new_item' => __('Add New Activity'),
        'edit_item' => __('Edit Activity'),
        'new_item' => __('New Activity'),
        'view_item' => __('View Activity'),
        'search_items' => __('Search Activities'),
        'not_found' => __('No Activities found'),
        'not_found_in_trash' => __('No Activities found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'activities', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-location', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'can_export' => true,
        'has_archive' => true,
    );
    register_post_type('activities', $args);
}

add_action('init', 'create_activities_category', 0);

function create_activities_category() {
    $labels = array(
        'name' => _x('Activity Categories', 'taxonomy general name'),
        'singular_name' => _x('Activity Category', 'taxonomy singular name'),
        'search_items' => __('Search Activity Categories'),
        'all_items' => __('All Activity Categories'),
        'parent_item' => __('Parent Activity Category'),
        'parent_item_colon' => __('Parent Activity Category:'),
        'edit_item' => __('Edit Activity Category'),
        'update_item' => __('Update Activity Category'),
        'add_new_item' => __('Add New Activity Category'),
        'new_item_name' => __('New Activity Category Name'),
        'menu_name' => __('Activity Categories'),
    );
    register_taxonomy('activities_category', 'activities', array(
        'public' => true,
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array(
            'slug' => 'see-do',
            'hierarchical' => true,
            'with_front' => false
        ),
    ));
}

/***************************************************************
 * Accommodations custom post type
 ***************************************************************/
add_action('init', 'register_accommodations');

function register_accommodations() {
    $labels = array(
        'name' => __('Accommodations'),
        'menu_name' => __('Accommodations'),
        'singular_name' => __('Accommodation'),
        'add_new_item' => __('Add New Accommodation'),
        'edit_item' => __('Edit Accommodation'),
        'new_item' => __('New Accommodation'),
        'view_item' => __('View Accommodation'),
        'search_items' => __('Search Accommodations'),
        'not_found' => __('No Accommodations found'),
        'not_found_in_trash' => __('No Accommodations found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'accommodations', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-admin-home', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'can_export' => true,
        'has_archive' => true,
    );
    register_post_type('accommodations', $args);
}

add_action('init', 'create_accommodations_category', 0);

function create_accommodations_category() {
    $labels = array(
        'name' => _x('Accommodation Categories', 'taxonomy general name'),
        'singular_name' => _x('Accommodation Category', 'taxonomy singular name'),
        'search_items' => __('Search Accommodation Categories'),
        'all_items' => __('All Accommodation Categories'),
        'parent_item' => __('Parent Accommodation Category'),
        'parent_item_colon' => __('Parent Accommodation Category:'),
        'edit_item' => __('Edit Accommodation Category'),
        'update_item' => __('Update Accommodation Category'),
        'add_new_item' => __('Add New Accommodation Category'),
        'new_item_name' => __('New Accommodation Category Name'),
        'menu_name' => __('Accommodation Categories'),
    );
    register_taxonomy('accommodations_category', 'accommodations', array(
        'public' => true,
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => false
    ));
}

/***************************************************************
 * Transportation custom post type
 ***************************************************************/
add_action('init', 'register_transportation');

function register_transportation() {
    $labels = array(
        'name' => __('Transportation'),
        'menu_name' => __('Transportation'),
        'singular_name' => __('Transportation'),
        'add_new_item' => __('Add New Transportation'),
        'edit_item' => __('Edit Transportation'),
        'new_item' => __('New Transportation'),
        'view_item' => __('View Transportation'),
        'search_items' => __('Search Transportation'),
        'not_found' => __('No Transportation found'),
        'not_found_in_trash' => __('No Transportation found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'transportation', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-location-alt', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'can_export' => true,
        'has_archive' => true,
    );
    register_post_type('transportation', $args);
}

add_action('init', 'create_transportation_category', 0);

function create_transportation_category() {
    $labels = array(
        'name' => _x('Transportation Categories', 'taxonomy general name'),
        'singular_name' => _x('Transportation Category', 'taxonomy singular name'),
        'search_items' => __('Search Transportation Categories'),
        'all_items' => __('All Transportation Categories'),
        'parent_item' => __('Parent Transportation Category'),
        'parent_item_colon' => __('Parent Transportation Category:'),
        'edit_item' => __('Edit Transportation Category'),
        'update_item' => __('Update Transportation Category'),
        'add_new_item' => __('Add New Transportation Category'),
        'new_item_name' => __('New Transportation Category Name'),
        'menu_name' => __('Transportation Categories'),
    );
    register_taxonomy('transportation_category', 'transportation', array(
        'public' => true,
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => false
    ));
}

/***************************************************************
 * Create Region Category -- used in Offers, Accommodations, Transportation, Events
 ***************************************************************/

add_action('init', 'create_region_category', 0);

function create_region_category() {
    $labels = array(
        'name' => _x('Region Categories', 'taxonomy general name'),
        'singular_name' => _x('Region Category', 'taxonomy singular name'),
        'search_items' => __('Search Region Categories'),
        'all_items' => __('All Region Categories'),
        'parent_item' => __('Parent Region Category'),
        'parent_item_colon' => __('Parent Region Category:'),
        'edit_item' => __('Edit Region Category'),
        'update_item' => __('Update Region Category'),
        'add_new_item' => __('Add New Region Category'),
        'new_item_name' => __('New Region Category Name'),
        'menu_name' => __('Region Categories'),
    );
    register_taxonomy('region_category', array('offers', 'accommodations', 'transportation', 'events'),
        array(
            'public' => true,
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
        ));
}


/***************************************************************
 * Events custom post type
 ***************************************************************/
add_action('init', 'register_events');

function register_events() {
    $labels = array(
        'name' => __('Events'),
        'menu_name' => __('Events'),
        'singular_name' => __('Event'),
        'add_new_item' => __('Add New Event'),
        'edit_item' => __('Edit Event'),
        'new_item' => __('New Event'),
        'view_item' => __('View Event'),
        'search_items' => __('Search Events'),
        'not_found' => __('No Events found'),
        'not_found_in_trash' => __('No Events found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'events', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-calendar', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
    );
    register_post_type('events', $args);
}

/***************************************************************
 * Press Releases custom post type
 ***************************************************************/
add_action('init', 'register_press_releases');

function register_press_releases() {
    $labels = array(
        'name' => __('Press Releases'),
        'menu_name' => __('Press Releases'),
        'singular_name' => __('Press Release'),
        'add_new_item' => __('Add New Press Release'),
        'edit_item' => __('Edit Press Release'),
        'new_item' => __('New Press Release'),
        'view_item' => __('View Press Release'),
        'search_items' => __('Search Press Releases'),
        'not_found' => __('No Press Releases found'),
        'not_found_in_trash' => __('No Press Releases found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'thumbnail', 'revisions'),
        'rewrite' => array('slug' => 'press', 'with_front' => false),
        'capability_type' => 'post',
        'menu_position' => 20, // after Pages
        'menu_icon' => 'dashicons-post-status', // http://calebserna.com/dashicons-cheatsheet/
        'hierarchical' => false,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
    );
    register_post_type('press_releases', $args);
}