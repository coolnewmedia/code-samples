<?php

/***************************************************************
 * Register Menus
 ***************************************************************/
if (function_exists('register_nav_menus')) {
    function register_theme_menus() {
        register_nav_menus(
            array(
                'header_primary_nav' => __('Header Primary Nav'),
                //'header_secondary_nav' => __('Header Secondary Nav'),
                'footer_primary_nav' => __('Footer Primary Nav'),
                //'footer_secondary_nav' => __('Footer Secondary Nav'),
                //'footer_tertiary_nav' => __('Footer Tertiary Nav')
            )
        );
    }

    add_action('init', 'register_theme_menus');
}

/** Register Custom Navigation Walker **/
require_once get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/*** Add classes to menu **/
function add_classes_on_li($classes, $item, $args) {
    $classes[] = 'nav-item';
    return $classes;
}

add_filter('nav_menu_css_class', 'add_classes_on_li', 1, 3);

function add_classes_on_a($ulclass) {
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}

add_filter('wp_nav_menu', 'add_classes_on_a');


/**
 * Use wp_list_pages() to display parent and all child pages of current page.
 */
function wpse_get_ancestor_tree() {
    // Bail if this is not a page.
    if ( ! is_page() ) {
        return false;
    }

    // Get the current post.
    $post = get_post();

    /**
     * Get array of post ancestor IDs.
     * Note: The direct parent is returned as the first value in the array.
     * The highest level ancestor is returned as the last value in the array.
     * See https://codex.wordpress.org/Function_Reference/get_post_ancestors
     */
    $ancestors = get_post_ancestors( $post->ID );

    // If there are ancestors, get the top level parent.
    // Otherwise use the current post's ID.
    $parent = ( ! empty( $ancestors ) ) ? array_pop( $ancestors ) : $post->ID;

    // Get all pages that are a child of $parent.
    $pages = get_pages( [
        'child_of' => $parent,
    ] );

    // Bail if there are no results.
    if ( ! $pages ) {
        return false;
    }

    // Store array of page IDs to include latere on.
    $page_ids = array();
    foreach ( $pages as $page ) {
        $page_ids[] = $page->ID;
    }

    // Add parent page to beginning of $page_ids array.
    array_unshift( $page_ids, $parent );

    // Get the output and return results if they exist.
    $output = wp_list_pages( [
        'include'  => $page_ids,
        'title_li' => false,
        'echo'     => false,
    ] );

    if ( ! $output ) {
        return false;
    } else {
        return '<ul class="page-menu ancestor-tree">' . PHP_EOL .
            $output . PHP_EOL .
            '</ul>' . PHP_EOL;
    }
}

