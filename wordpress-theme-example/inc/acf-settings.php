<?php

/***************************************************************
 * Layout automation for ACF's Flexible Content
 ***************************************************************/
require_once locate_template('/acf-layout-class.php');

add_action('admin_menu', 'remove_post_custom_fields_metabox');