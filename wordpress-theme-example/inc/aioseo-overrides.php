<?php /**
 * All in One SEO -- functionality overrides
 * Page Titles for individual Archive Pages  --because this is not supported well for WP CPT archives pages
 * Fetch the Archive page title out of General Settings
 */


/**
 * Archive page Title
 * @param $pagetitle
 * @return mixed
 */
function archive_pagetitle($pagetitle) {

    if (is_archive(
        array(
            'events',
            'offers',
            'accommodations'
        )
    )) {
        if (is_post_type_archive('accommodations')) {
            $pagetitle = get_field('accommodations_page_title', 'options');
        } elseif (is_post_type_archive('events')) {
            $pagetitle = get_field('events_page_title', 'options');
        } elseif (is_post_type_archive('offers')) {
            $pagetitle = get_field('offers_page_title', 'options');
        }
        // We can also do other things here, like remove the canonical tag, for example...
        remove_action('wp_head', 'rel_canonical');
    }
    return $pagetitle;
}

add_filter('aioseop_title', 'archive_pagetitle', 1);

/**
 * Archive page meta description
 * @param $description
 * @return mixed
 */
function archive_description($description) {

    if (is_archive(
        array(
            'events',
            'offers',
            'accommodations'
        )
    )) {
        if (is_post_type_archive('accommodations')) {
            $description = get_field('accommodations_meta_description', 'options');
        } elseif (is_post_type_archive('events')) {
            $description = get_field('events_meta_description', 'options');
        } elseif (is_post_type_archive('offers')) {
            $description = get_field('offers_meta_description', 'options');
        }
    }
    return $description;
}

add_filter('aioseop_description', 'archive_description', 1);

/**
 * Add a canonical URL to each activities category taxonomy archive that is missing a canonical URL
 * @param $url
 * @return string
 */
function change_canonical_url($url) {
  if ( is_tax('activities_category') && !$url ) {
    global $wp;
    return home_url( $wp->request );
  }
  return $url;
}

add_filter('aioseop_canonical_url', 'change_canonical_url', 10, 1);