<?php

/***************************************************************
 * Remove the main content from pages because we're using ACF
 ***************************************************************/
function remove_editor() {
    remove_post_type_support('page', 'editor');
}

add_action('init', 'remove_editor');


/***************************************************************
 * CUSTOM MCE SETTINGS - Custom Classes for Wordpress WYSIWYG
 ***************************************************************/
function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function mce_additional_formats($init_array) {
    $style_formats = array(
        array(
            'title' => 'Headings',
            'items' => array(
                array(
                    'title' => 'Heading 1',
                    'selector' => '',
                    'classes' => 'h1',
                ),
                array(
                    'title' => 'Heading 2',
                    'selector' => '',
                    'classes' => 'h2',
                ),
                array(
                    'title' => 'Heading 3',
                    'selector' => '',
                    'classes' => 'h3',
                ),
                array(
                    'title' => 'Heading 4',
                    'selector' => '',
                    'classes' => 'h4',
                ),
                array(
                    'title' => 'Heading 5',
                    'selector' => '',
                    'classes' => 'h5',
                ),
                array(
                    'title' => 'Heading 6',
                    'selector' => '',
                    'classes' => 'h6',
                ),
            ),
        ),
        array(
            'title' => 'Lists',
            'items' => array(
                array(
                    'title' => 'List (Basic)',
                    'selector' => 'ul,ol',
                    'classes' => 'list-basic',
                ),
            ),
        ),
        array(
            'title' => 'Buttons',
            'items' => array(
                array(
                    'title' => 'Block',
                    'selector' => 'a',
                    'classes' => 'btn',
                ),
                array(
                    'title' => 'Text',
                    'selector' => 'a',
                    'classes' => 'txt-btn',
                ),
            ),
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'mce_additional_formats');

/***************************************************************
 * Removing emojis
 ***************************************************************/
function disable_wp_emojicons() {
    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // filter to remove TinyMCE emojis
    add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');

    // Remove the DNS prefetch by returning false on filter emoji_svg_url
    add_filter('emoji_svg_url', '__return_false');
}

add_action('init', 'disable_wp_emojicons');

function disable_emojicons_tinymce($plugins) {
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

