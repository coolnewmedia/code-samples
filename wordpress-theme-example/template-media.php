<?php
/* Template Name: Media Center Page with Sidebar */
get_header();

$content = get_field('content');

$default_post_image = get_field('default_post_preview_image', 'option');

$query = new WP_Query(array(
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'post_type' => 'press_releases'
)); ?>

    <div class="basic-page-with-sidebar">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png"
             alt="ripple background"/>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <h1><?php the_title(); ?></h1>

                    <?php echo $content; ?>

                    <?php // Show the Press Releases ?>
                    <?php if ($query->have_posts()) : ?>
                        <section class="press-releases">
                            <div class="archive-list__heading">
                                <h2>Press Releases</h2>
                            </div>
                            <div class="archive-list__container">

                                <?php while ($query->have_posts()) :
                                    $query->the_post();
                                    $content = get_field('content');

                                    $fly_image = fly_get_attachment_image_src(get_post_thumbnail_id(), 'listing_thumbnail', true);
                                    $fly_image_alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
                                    ?>

                                    <div class="archive-item">
                                        <div class="archive-item__inner">
                                            <div class="archive-item__image">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php if (has_post_thumbnail()) : ?>
                                                        <img class="lazyload"
                                                             data-src="<?php echo $fly_image['src']; ?>"
                                                             src="/content/themes/base/img/placeholder.gif"
                                                             alt="<?php echo $fly_image_alt; ?>"/>
                                                    <?php elseif ($default_post_image) : ?>
                                                        <img class="lazyload"
                                                             data-src="<?php echo $default_post_image['url']; ?>"
                                                             src="/content/themes/base/img/placeholder.gif"
                                                             alt="<?php echo $default_post_image['alt']; ?>"/>
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="archive-item__content">
                                                <div class="archive-item__content-group archive-item__content-first">
                                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                    <?php if ($content) : ?>
                                                        <?php echo wp_trim_words($content, 20, '...'); ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="archive-item__content-group archive-item__content-last ">
                                                    <a class="btn" href="<?php the_permalink(); ?>">
                                                        Read More
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>

                        </section>
                        <?php wp_reset_query(); ?>
                    <?php endif; ?>

                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 sidebar">
                    <?php echo wpse_get_ancestor_tree(); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();