<?php

$instagram_feed_heading = get_field('instagram_feed_heading', 'options');

$footer_menu_text = get_field('footer_text', 'option');

$social_media_heading = get_field('social_media_heading', 'option');

$usa_image = get_field('logo_usa', 'option');
$usa_link = get_field('logo_usa_link', 'option');
$national_geographic_image = get_field('logo_national_geographic', 'option');
$national_geographic_link = get_field('logo_national_geographic_link', 'option');

$vacation_guide_pdf = get_field('vacation_guide_pdf', 'options');
$vacation_guide_image = get_field('vacation_guide_image', 'options');
$vacation_guide_heading = get_field('vacation_guide_heading', 'options');
$vacation_guide_download_heading = get_field('vacation_guide_download_heading', 'options');
$vacation_guide_footer_download_text = get_field('vacation_guide_footer_download_text', 'options');

$travel_storys_logo = get_field('travel_storys_logo', 'option');
$travel_storys_link = get_field('travel_storys_link', 'option');
$travel_storys_text = get_field('travel_storys_text', 'option');

$copyright = get_field('copyright', 'option');

?>

<?php /** Crowdriff Feed */ ?>

<?php // @todo Need to refactor this logic as it is a bit cumbersome to understand ?>
<?php if (
    !is_singular(
        array(
            'post',
            'offers',
            'activities',
            'accommodations',
            'transportation'
        ))
    and !is_archive()
    and !is_home()
    and !is_page(
        array(
            'request-information',
            'terms-of-use',
            'privacy-policy',
            'contact-us',
            'getting-here',
            'thank-you'
        ))) : ?>

    <div class="instagram-feed">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png"
             alt="ripple background"/>
        <?php if ($instagram_feed_heading) : ?>
            <div class="col-sm-12 col-md-10 offset-md-1">
                <div class="social-links">
                    <h2 class="instagram-feed-heading"><?php echo $instagram_feed_heading; ?></h2>
                    <?php include(locate_template('template-parts/social-links.php', false, false)); ?>
                </div>
            </div>
        <?php endif; ?>
        <script id="cr__init-e5e4ac8e" src="https://embed.crowdriff.com/js/init?hash=e5e4ac8e" async></script>
    </div>
<?php endif; ?>

<?php /** Newsletter Sign-up form */ ?>
<?php if (!is_page(
    array(
        'request-information',
        'thank-you'
    ))) : ?>

    <?php include(locate_template('template-parts/newsletter-signup.php', false, false)); ?>

<?php endif; ?>

<footer>
    <div class="logo text-center">
        <?php if (!is_front_page()) : ?>
            <a href="/" class="logo-home" title="Home">
                <span class="screen-reader-text"><?php echo get_bloginfo('name'); ?></span>
                <?php include(locate_template('template-parts/logo.svg.php', false, false)); ?>
            </a>
        <?php else : ?>
            <?php include(locate_template('template-parts/logo.svg.php', false, false)); ?>
            <!--                <span>--><?php //echo get_bloginfo('name'); ?><!--</span>-->
        <?php endif; ?>
    </div>

    <?php if ($footer_menu_text) : ?>
        <p class="text-center"><?php echo $footer_menu_text; ?></p>
    <?php endif; ?>
    <?php
    wp_nav_menu(array(
        'menu' => 'primary footer',
        'theme_location' => 'Footer Primary Nav',
        'depth' => 1,
        'container' => 'div',
        'container_id' => 'footer-menu',
        'menu_class' => 'nav',
        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
        'walker' => new WP_Bootstrap_Navwalker(),
    ));
    ?>
    <div class="social-links text-center">
        <?php /** Social Links */ ?>
        <?php if ($social_media_heading) : ?>
            <h4><?php echo $social_media_heading; ?></h4>
        <?php endif; ?>
        <?php include(locate_template('template-parts/social-links.php', false, false)); ?>
    </div>

    <div class="container">
        <div class="travel-storys row">
            <div class="travel-storys__logo">
                <?php if ($travel_storys_logo) : ?>
                    <a href="<?php echo $travel_storys_link; ?>" target="_blank">
                        <img class="lazyload" src="" data-src="<?php echo $travel_storys_logo['url']; ?>"
                             alt="<?php echo $travel_storys_logo['alt']; ?>"/>
                    </a>
                <?php endif; ?>
            </div>
            <div class="travel-storys__text">
                <?php echo $travel_storys_text; ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="footer-logos row">
            <div class="logo">
                <?php if ($usa_image) : ?>
                    <a href="<?php echo $usa_link; ?>" target="_blank">
                        <img class="lazyload" src="" data-src="<?php echo $usa_image['url']; ?>"
                             alt="<?php echo $usa_image['alt']; ?>"/>
                    </a>
                <?php endif; ?>
            </div>
            <div class="logo">
                <?php if ($national_geographic_image) : ?>
                    <a href="<?php echo $national_geographic_link ?>" target="_blank">
                        <img class="lazyload" src="" data-src="<?php echo $national_geographic_image['url']; ?>"
                         alt="<?php echo $national_geographic_image['alt']; ?>"/>
                    </a>
                <?php endif; ?>
            </div>
            <div class="logo vacation-guide-footer">
                <?php if ($vacation_guide_pdf) : ?>
                    <div class="vacation-guide-footer__image">
                        <?php if ($vacation_guide_image) : ?>
                            <img class="lazyload" src="" data-src="<?php echo $vacation_guide_image['url']; ?>"
                                 alt="<?php echo $vacation_guide_image['alt']; ?>"/>
                        <?php endif; ?>
                    </div>
                    <div class="vacation-guide-footer__content">
                        <?php if ($vacation_guide_heading) : ?>
                            <?php echo $vacation_guide_heading; ?>
                            <p role="button" class="download" data-toggle="modal" data-target="#vacationGuideDownload">
                                <?php echo $vacation_guide_footer_download_text; ?>
                            </p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <div class="copyright text-center p-3">
        <?php echo $copyright; ?>
    </div>

</footer>

<?php
// Modal Pop-up for the Vacation Guide:
// -Users fill out a form before they have access to the download.
// -Added in the Footer because is should be available from all pages.
?>
<div class="modal fade" id="vacationGuideDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 vacation-guide__image">
                            <img class="lazyload"
                                 data-src="<?php echo $vacation_guide_image['url']; ?>"
                                 src="/content/themes/base/img/placeholder.gif"
                                 alt="<?php echo $vacation_guide_image['alt']; ?>"/>
                        </div>
                        <div class="col-sm-6">
                            <h2><?php echo $vacation_guide_download_heading; ?></h2>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <?php echo do_shortcode("[gravityform id=5 title=false description=false ajax=true tabindex=49]"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

// Footer scripts and magically make it all work stuff.

wp_footer();

?>


<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>

<?php $belowFoldJS = '/js/below-fold.js'; ?>

<?php if (file_exists(dirname(__FILE__) . $belowFoldJS)) : ?>
    <?php $belowFoldJSMtime = filemtime(dirname(__FILE__) . $belowFoldJS); ?>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?><?php echo $belowFoldJS; ?>?v=<?php echo $belowFoldJSMtime; ?>"></script>
<?php endif; ?>


<script>
    if (typeof $.fn.fancybox == 'function') {
        $('[data-fancybox]').fancybox();
    }
</script>


<?php

if (!in_array($_SERVER['HTTP_HOST'], $allowed_hosts)) :
    include(locate_template('template-parts/development.php', false, false));
endif;
?>


</div>
</body>
</html>