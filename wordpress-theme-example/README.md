# A sample of the Theme files for the a travel destination website build

The Agency I worked for often tasked 1-4 developers to build out a site.  This site did not have a lot of back-end work so it was task to myself and another front-end developer.

Files included contain a majority of my coding.  I spend 3 months working this build.   The coding represents some best practices and some non-optimal coding that came as a result of project changes.

As front-end developers, we became intimate with this SASS code and it went through a few iterations and rewording based on client feedback and code cleanup/optimization. 

## Some key features:

* Bootstrap 4
* CSS animations  (menu hamburger)
* Transitions and fades
* Lazy loading and optimized CSS loading/delivering

The current site represents a lot of the original work but as all sites enter maintenance mode, changes have been added but new developers.  It appears something has happening with the loading of the hero video.  Site was not released like that.
