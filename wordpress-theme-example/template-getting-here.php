<?php
/* Template Name: Getting Here Page with Sidebar */

get_header();

$content = get_field('content');

$transportation_heading = get_field('transportation_heading_text', 'option');

$categories = get_terms(array(
	'taxonomy' => 'transportation_category',
	'hide_empty' => false,
));


$region_categories = get_terms(array(
	'taxonomy' => 'region_category',
	'hide_empty' => false,
));

$query = new WP_Query(array(
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'post_type' => 'transportation'
));

$taxonomy = 'transportation_category';
$post_type_name = 'transportation';

?>


	<div class="basic-page-with-sidebar">
		<img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png" alt="ripple background" />
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8 col-lg-9">
					<h1><?php the_title(); ?></h1>
					<?php echo $content; ?>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-3 sidebar">
					<?php echo wpse_get_ancestor_tree(); ?>
				</div>
			</div>
		</div>
	</div>

<?php

// Include the I-90 Shuffle layout if the date is between Apr 01 and Nov 01.  Reason is that yellowstone part of the route is closed in winter and map points don't work.

$today = date("m-d");
$yearstart = "01-01";
$date1 = "04-01";
$date2 = "11-01";
$yearend = "12-31";

// This page includes the I-90 layout however the heading variable cannot be set.  So we set it manually.
// @todo Figure out a way to set the heading when the ACF layout is included, or enable the page to accept page layouts.

$heading = "Discover<br>The I-90 Shuffle ";

if (!($today >= $yearstart && $today <= $date1) && !($today >= $date2 && $today <= $yearend)) : ?>

    <?php include(locate_template('acf-layouts/i-90_shuffle.php', false, false)); ?>

<?php endif; ?>

<?php if ($query->have_posts()) : ?>

	<section class="transportation">
		<img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged--mountain.png" alt="ripple background"/>
		<div class="container archive-list">
			<div class="row">
				<div class="col-sm-12">

					<h2 class="text-center"><?php echo $transportation_heading; ?></h2>

					<?php include(locate_template('template-parts/ajax-filter-cards.php', false, false)); ?>

				</div>
			</div>
		</div>
	</section>

<?php endif; ?>


<?php get_footer();