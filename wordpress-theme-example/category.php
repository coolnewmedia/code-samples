<?php

get_header();

?>

    <div id="hero" class="hero-short">
        <div class="item">
            <?php if ($hero_image) : ?>
                <?php $image_desktop = fly_get_attachment_image_src($hero_image['id'], 'hero_desktop'); ?>
                <?php $image_tablet = fly_get_attachment_image_src($hero_image['id'], 'hero_tablet'); ?>
                <?php $image_mobile = fly_get_attachment_image_src($hero_image['id'], 'hero_mobile'); ?>

                <picture>
                    <source data-srcset="<?php echo $image_desktop['src']; ?>" media="(min-width: 1440px)"
                            srcset="<?php echo $image_desktop['src']; ?>">
                    <source data-srcset="<?php echo $image_tablet['src']; ?>" media="(min-width: 768px)"
                            srcset="<?php echo $image_tablet['src']; ?>">
                    <source data-srcset="<?php echo $image_mobile['src']; ?>" media="(min-width: 0px)"
                            srcset="<?php echo $image_mobile['src']; ?>">
                    <img class="lazyload" data-src="<?php echo $image_desktop['src']; ?>"
                         alt="<?php echo $hero_image['alt']; ?>" src="<?php echo $image_desktop['src']; ?>">
                </picture>
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg" alt="Scenic lake view of the Country with rolling hills and mountains in the distance"/>
            <?php endif; ?>
        </div>
    </div>
    <div class="archive-posts-category default-archive archive">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png" alt="ripple background"/>
        <div class="container archive-list">
            <div class="row">
                <div class="col-sm-12">
                    <div class="archive-list__heading text-center">
                        <h1>Stories from our <i>Travel Blog</i></h1>
                        <h2><?php single_cat_title(); ?></h2>
                    </div>

                    <?php if (have_posts()) : ?>
                        <div class="archive-list__container">
                            <?php while (have_posts()) : the_post();
                                $website = get_field('website');
                                $phone_number = get_field('phone_number');
                                $details = get_field('details');
                                $content = get_field('content');
                                ?>
                                <div class="archive-item">
                                    <div class="archive-item__inner">
                                        <div class="archive-item__image">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php if (has_post_thumbnail()) : ?>
                                                    <?php echo fly_get_attachment_image(get_post_thumbnail_id(), 'listing_thumbnail'); ?>
                                                <?php elseif ($default_post_image) : ?>
                                                    <?php echo fly_get_attachment_image($default_post_image['id'], 'listing_thumbnail'); ?>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="archive-item__content">
                                            <h3><?php the_title(); ?></h3>
                                            <?php if ($content) : ?>
                                                <?php echo wp_trim_words($content, 20, '...'); ?>
                                            <?php endif; ?>
                                            <?php if ($phone_number) : ?>
                                                <div class="phone-number">Phone: <?php echo $phone_number; ?></div>
                                            <?php endif; ?>
                                            <div class="archive-item__buttons">
                                                <?php if ($website) : ?>
                                                    <a class="website"
                                                       target="_blank"
                                                       href="<?php echo $website; ?>"><?php echo $website; ?></a>
                                                <?php endif; ?>
                                                <a href="<?php the_permalink(); ?>">
                                                    View Details
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    <?php else: ?>
                        No <?php echo $post_type_name; ?> found.
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();