<?php

get_header();

$data = array();
$data['layout_num'] = 0;

// Check if ACF is installed and loop through custom flex layouts
if (function_exists('has_sub_field')):
    while (have_rows('page_layouts')) : // Loop through Flexible Content
        the_row();
        // Send $data as variable so we can use each field as a variable immediately.
        $data['row'] = get_row(true);
        $data['layout_num']++;
        ACF_Layout::render(get_row_layout(), $data);
    endwhile;
endif;

$args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'taxonomy' => 'activities_category',
    'hide_empty' => false,
    'parent' => 0
);

$categories = get_terms($args);

// Default image
$default_post_image = get_field('default_post_preview_image', 'option');
$fly_default = fly_get_attachment_image_src($default_post_image['id'], 'listing_thumbnail', true);

?>

    <section class="activity-cats textured--tan">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/rugged--mountain.png" alt="ripple background"/>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="activity-cats__container">
                        <?php foreach ($categories as $category) : ?>
                            <div class="cat">
                                <div class="cat__inner">
                                    <?php

                                    // Category Thumb
                                    $thumbnail = get_field('thumbnail', $category);
                                    $fly_image = fly_get_attachment_image_src($thumbnail['id'], 'listing_thumbnail', true);
                                    $fly_image_alt = get_post_meta($thumbnail['id'], '_wp_attachment_image_alt', true);

                                    ?>
                                    <div class="cat__image">
                                        <a href="<?php echo $category->slug; ?>/" title="<?php echo $category->name; ?>">
                                            <?php if ($thumbnail) : ?>
                                                <img class="lazyload"
                                                     data-src="<?php echo $fly_image['src']; ?>"
                                                     src="/content/themes/base/img/placeholder.gif"
                                                     alt="<?php echo $fly_image_alt; ?>"/>
                                            <?php elseif ($default_post_image) : ?>
                                                <img class="lazyload"
                                                     data-src="<?php echo $fly_default['src']; ?>"
                                                     src="/content/themes/base/img/placeholder.gif"
                                                     alt="<?php echo $default_post_image['alt']; ?>"/>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="cat__content">
                                        <h3><a href="<?php echo $category->slug; ?>/" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a></h3>
                                        <?php if ($category->description) :
                                            echo $category->description;
                                        endif; ?>
                                        <a class="btn" href="<?php echo $category->slug; ?>/" title="<?php echo $category->name; ?>">
                                            <?php _e('Learn More');?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();