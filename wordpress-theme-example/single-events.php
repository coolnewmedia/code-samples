<?php

/**
 * Single: Event
 * Custom Template for events pages
 *
 */

get_header();

$hero_image = get_field('hero_image');
$default_single_post_hero = get_field('default_single_post_hero_image', 'option');

if (have_posts()) : while (have_posts()) : the_post() ?>

    <div id="hero" class="hero-short">
        <div class="item">
            <?php if ($hero_image) : ?>
                <img src="<?php echo $hero_image['url']; ?>" />
            <?php elseif ($default_single_post_hero) : ?>
                <img src="<?php echo $default_single_post_hero['url']; ?>" />
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg" alt="Scenic lake view of the Country with rolling hills and mountains in the distance" />
            <?php endif; ?>
        </div>
    </div>

<article class="single-event <?php if (has_post_thumbnail()) : echo 'has-image'; endif; ?>">
    <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png" alt="ripple background" />
    <div class="container">
        <div class="row no-gutters">
            <div class="col-sm-12 col-md-7 ">
                <h1><?php the_title(); ?></h1>
                <?php
                    $start_date = get_field('start_date');
                    $end_date = get_field('end_date');
                    $time = get_field('time');
                    $location = get_field('location');
                    $phone_number = get_field('phone_number');
                    $contact = get_field('contact');
                    $website = get_field('website');
                    $details = get_field('details');
                ?>
                <div class="event-info">
                    <?php if ($start_date && $end_date) : ?>
                        <div class="start-date"><span>Start Date:</span> <?php echo $start_date; ?></div>
                        <div class="end-date"><span>End Date:</span> <?php echo $end_date; ?></div>
                    <?php elseif ($start_date) : ?>
                        <div class="start-date"><span>Date:</span> <?php echo $start_date; ?></div>
                    <?php endif; ?>

                    <?php if ($time) : ?>
                        <div class="time"><span>Time:</span> <?php echo $time; ?></div>
                    <?php endif; ?>

                    <?php if ($location) : ?>
                        <div class="location"><span>Location:</span> <?php echo $location; ?></div>
                    <?php endif; ?>

                    <?php if ($phone_number) : ?>
                        <div class="phone-number"><span>Phone Number:</span> <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></div>
                    <?php endif; ?>

                    <?php if ($contact) : ?>
                        <div class="contact"><span>Contact:</span> <?php echo $contact; ?></div>
                    <?php endif; ?>

                    <?php if ($website) : ?>
                        <div class="website"><span>Website:</span> <a href="<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a></div>
                    <?php endif; ?>
                </div>

                <?php if ($details) : echo $details; endif; ?>
            </div>
            <?php if (has_post_thumbnail()) : ?>
                <div class="col-sm-12 col-md-4 offset-md-1 pr-md-0">
                    <div class="post__image">
                        <?php echo fly_get_attachment_image( get_post_thumbnail_id(), 'listing_thumbnail' ); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>
<?php endwhile; endif; ?>

<?php if (! is_singular('transportation')) :
    include(locate_template('template-parts/related-posts.php', false, false));
endif; ?>

<?php get_footer();