<?php
/**
 * Primary Menu in Header
 */

// Bring in the DarkSky weather forecast
global $forecast;

?>

<nav class="navbar navbar-expand-md" role="navigation">
    <div class="navbar-brand logo nav__left">
        <?php if (!is_front_page()) : ?>
            <a href="/" class="logo-home" title="<?php echo get_bloginfo('name'); ?>">
                <span class="screen-reader-text"><?php echo get_bloginfo('name'); ?></span>
                <?php include(locate_template('template-parts/logo.svg.php', false, false)); ?>
            </a>
        <?php else : ?>
            <?php include(locate_template('template-parts/logo.svg.php', false, false)); ?>
        <?php endif; ?>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-menu"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <input type="checkbox" name="toggle-menu" title="Show hide Mobile Menu"/>
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="menu">Menu</span>
    </button>
    <?php
    wp_nav_menu(array(
        'menu' => 'primary menu',
        'theme_location' => 'Header Primary Nav',
        'depth' => 1,
        'container' => 'div',
        'container_class' => 'collapse navbar-collapse nav__center',
        'container_id' => 'primary-menu',
        'menu_class' => 'nav navbar-nav',
        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
        'walker' => new WP_Bootstrap_Navwalker(),
    ));
    ?>
    <div class="nav__right">

        <div class="weather-widget">

            <?php // Check if we have a forecast ?>
            <?php if ($forecast) : ?>
                <a href="/weather/" title="<?php echo $forecast['currently']['summary']; ?>">
                    <span class="icon"><?php echo file_get_contents(locate_template('/img/weather-icons/' . getWeatherIcon($forecast))); ?></span><span
                            class="temp"><?php echo round($forecast['currently']['temperature'], 0, PHP_ROUND_HALF_UP); ?>&deg;F</span>
                </a>
            <?php endif; ?>

        </div>
        <div class="request-info">
            <a href="/request-information/">Request Info</a>
        </div>
    </div>
</nav>


