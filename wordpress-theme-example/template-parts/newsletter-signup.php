<?php
/**
 * Newsletter Sign-up
 */

$heading = get_field('newsletter_heading', 'option');

?>
<section class="newsletter-signup">
    <div class="text-center container">
        <div class="row">
            <div class="newsletter-signup__form col-sm-12 col-xl-10">
                <?php if ($heading) : ?>
                    <h2><?php echo $heading; ?></h2>
                <?php endif; ?>
                <?php gravity_form(1, false, false, false, '', true, 12); ?>
            </div>
        </div>
    </div>
</section>