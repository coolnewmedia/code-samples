
<?php
// check if the repeater field has rows of data
if (have_rows('social_links', 'option')): ?>

<ul>


<?php
    // loop through the rows of data
    while (have_rows('social_links', 'option')) : the_row();

        // display a sub field value ?>
        <li>
            <a href="<?php the_sub_field('social_link_url'); ?>"
               alt="<?php the_sub_field('social_link_text'); ?>"
               title="<?php the_sub_field('social_link_text'); ?>"
               target="_blank">
                <span><?php the_sub_field('social_link_text'); ?></span>
                <i class="fab fa-<?php the_sub_field('social_link_icon'); ?>"></i>
            </a>
        </li>
    <?php

    endwhile;

else :

    // no rows found

    ?>
</ul>

<?php endif;
