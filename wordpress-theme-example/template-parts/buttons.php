<?php if (have_rows('buttons')) : ?>

    <div class="buttons">
        <?php // Setup of button based on button type

        while (have_rows('buttons')) : the_row();
            $button_type = get_sub_field('button_type');
            $label = get_sub_field('label');
            $video_link = get_sub_field('video_link');
            $external_link = get_sub_field('external_link');
            $internal_page = get_sub_field('internal_page');
            $file_to_download = get_sub_field('file_to_download');
            $css_class = get_sub_field('css_class');

            $data_attribute = "";
            $video_class = "";

            // Add some additional attributes if video
            if ($button_type == "video") :
                $data_attribute = 'data-fancybox="video"';
                $video_class = "video";
            endif;

            $link = 0;

            if ($button_type == 'internal') :
                $link = $internal_page;
            elseif ($button_type == 'video'):
                $link = $video_link;
            elseif ($button_type == 'download'):
                $link = $file_to_download['url'];
            else:
                $link = $external_link;
            endif; ?>

            <a <?php echo $data_attribute;?> href="<?php echo $link; ?>" class="btn<?php echo ' ' . $video_class;?><?php if ($css_class) : echo ' ' . $css_class; endif; ?>"
                <?php if ($button_type == 'external') :
                    echo 'target="_blank" rel="noopener"';
                elseif ($button_type == 'download'):
                    echo 'download';
                endif; ?>>
                <span><?php echo $label; ?></span>
            </a>
        <?php endwhile; ?>
    </div>
<?php endif; ?>