<?php

get_header();

$hero_image = get_field('hero_image');
$default_single_post_hero = get_field('default_single_post_hero_image', 'option');

if (have_posts()) :
    while (have_posts()) :

        the_post();

        $offer_title = get_field('offer_title');
        $partners = get_field('partner');
        $offered_by = get_field('offered_by');
        $business_name = get_field('business_name');
        $offer_details = get_field('offer_details');
        $expiry_date = get_field('expiry_date');
        $partner = $partners[0];

        ?>

        <div id="hero" class="hero-short">
            <div class="item">
                <?php if ($hero_image) : ?>
                    <img src="<?php echo $hero_image['url']; ?>"
                         alt="<?php $hero_image['alt']; ?>"/>
                <?php elseif ($default_single_post_hero) : ?>
                    <img src="<?php echo $default_single_post_hero['url']; ?>"
                         alt="<?php $default_single_post_hero['alt']; ?>"/>
                <?php else: ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg"
                         alt="Scenic lake view of the Country with rolling hills and mountains in the distance"/>
                <?php endif; ?>
            </div>
        </div>

        <?php

        if ($offered_by) :
            $address = get_field('address', $partner->ID);
            $phone_number = get_field('phone_number', $partner->ID);
            $email_address = get_field('email_address', $partner->ID);
            $website = get_field('website', $partner->ID);
        else:
            if (have_rows('contact_info')):
                while (have_rows('contact_info')): the_row();
                    $address = get_sub_field('address');
                    $phone_number = get_sub_field('phone_number');
                    $email_address = get_sub_field('email_address');
                    $website = get_sub_field('website');
                endwhile;
            endif;
        endif;

        // Check if the partner has the post thumbnail set

        if ($offered_by) :
            if (has_post_thumbnail($partner->ID)) :
                $partner_post_thumbnail = get_post_thumbnail_id($partner->ID);
            else:
                $partner_post_thumbnail = ""; // set it to nothing.
            endif;
        endif;

        ?>

        <article
                class="single-offer <?php if (has_post_thumbnail() || isset($partner_post_thumbnail)) : echo 'has-image'; endif; ?>">
            <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png"
                 alt="ripple background" alt="ripple background"/>
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-7">
                        <div class="offer-content">
                            <h1><?php if ($offer_title) : echo $offer_title; else: the_title(); endif; ?></h1>
                            <div class="offered-by">
                                Offered by:
                                <?php if ($offered_by) : ?>
                                    <a href="<?php echo get_the_permalink($partner->ID); ?>"><?php echo get_the_title($partner->ID); ?></a>
                                <?php else: ?>
                                    <?php echo $business_name; ?>
                                <?php endif; ?>
                            </div>

                            <div class="contact-info">
                                <?php if ($address) : ?>
                                    <div class="address"><span>Address:</span> <?php echo $address; ?></div>
                                <?php endif; ?>

                                <?php if ($phone_number) : ?>
                                    <div class="phone-number"><span>Phone Number:</span> <a
                                                href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a>
                                    </div>
                                <?php endif; ?>

                                <?php if ($email_address) : ?>
                                    <div><span>Email: </span><a class="email-address"
                                                                href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>
                                    </div>
                                <?php endif; ?>

                                <?php if ($website) : ?>
                                    <div class="website"><span>Website:</span> <a href="<?php echo $website; ?>"
                                                                                  target="_blank"><?php echo $website; ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="offer-details">
                                <?php echo $offer_details; ?>
                            </div>

                            <?php if ($expiry_date) : ?>
                                <div class="expiry-date">
                                    <i>*Offer expires <?php echo $expiry_date; ?></i>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>

                    <?php if (has_post_thumbnail() or isset($partner_post_thumbnail)) : ?>
                        <div class="col-sm-12 col-md-4 offset-md-1 pr-md-0">
                            <div class="offer__image">
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php echo fly_get_attachment_image(get_post_thumbnail_id(), 'listing_thumbnail'); ?>
                                <?php else: ?>
                                    <?php echo fly_get_attachment_image($partner_post_thumbnail, 'listing_thumbnail'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </article>

    <?php
    endwhile;
endif;

include(locate_template('acf-layouts/offers.php', false, false));

get_footer();