<?php

/**
 * Single: Post
 * Custom Template for Adventure Travel Blog individual pages
 *
 */

get_header();

?>

<?php if (have_posts()) : while (have_posts()) : the_post() ?>

    <article class="single-post <?php if (has_post_thumbnail()) : echo 'has-image'; endif; ?>">

        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png" alt="Ripple background"/>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-10 col-xl-8 article-info">
                    <h1><?php the_title(); ?></h1>
                    <div class="byline">Posted by: <strong><?php _e('Park County Travel Council'); ?></strong></div>
                    <div class="date">Posted on: <strong><?php the_date(); ?></strong></div>
                </div>
            </div>
        </div>


        <?php

        $data = array();
        $data['layout_num'] = 0;

        $post_layouts = have_rows('post_layouts');


        // Check if ACF is installed and loop through custom flex layouts
        if (function_exists('has_sub_field')):
            while (have_rows('post_layouts')) : // Loop through Flexible Content
                the_row();
                // Send $data as variable so we can use each field as a variable immediately.
                $data['row'] = get_row(true);
                $data['layout_num']++;
                ACF_Layout::render(get_row_layout(), $data);
            endwhile;

        endif;

        // If no ACF layout rows, get the_content(); out of the regular post content field.
        if (!$post_layouts) : ?>

            <div class="container wysiwyg">
                <div class="row">
                    <div class="col-sm-12 col-lg-10 col-xl-8">
                        <?php echo the_content(); ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>


        <div class="container sharethis">
            <hr/>
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <p>
                        <span class="share-text">Share this Story:</span>
                        <span data-network="facebook" class="st-custom-button"><i class="fab fa-facebook-f"></i></span>
                        <span data-network="twitter" class="st-custom-button"><i class="fab fa-twitter"></i></span>
                        <span data-network="pinterest" class="st-custom-button"><i class="fab fa-pinterest-p"></i></span>
                    </p>
                </div>
            </div>
        </div>

    </article>
<?php endwhile; ?>
<?php endif; ?>


<?php include(locate_template('template-parts/related-posts.php', false, false)); ?>

<?php include(locate_template('acf-layouts/offers.php', false, false)); ?>

<?php get_footer();