<?php


/**
 * Functions -------------------------------------------------
 * Separate out function sets to make functions more manageable
 *
 */

require_once(__DIR__ . '/inc/acf-settings.php');
require_once(__DIR__ . '/inc/menu-settings.php');
require_once(__DIR__ . '/inc/image-settings.php');
require_once(__DIR__ . '/inc/custom-post-types.php');
require_once(__DIR__ . '/inc/custom-mce-settings.php');
require_once(__DIR__ . '/inc/forms-settings.php');
require_once(__DIR__ . '/inc/ajax_filter.php');
require_once(__DIR__ . '/inc/aioseo-overrides.php');

/***************************************************************
 * Content width
 ***************************************************************/
//$content_width is a global variable used by WordPress for max image upload sizes and media embeds (in pixels).
if (!isset($content_width)) {
    $content_width = 1280;
}


/***************************************************************
 * Remove admin bar on live site to prevent being cached in cloudflare
 * This can be removed if you're not caching full html in cloudflare
 ***************************************************************/
//if( isset($_ENV) && isset($_ENV['VSRV_ENV']) && $_ENV['VSRV_ENV'] != 'DEV' ) {
//	show_admin_bar(false);
//}


/***************************************************************
 * Handle the custom scripts
 ***************************************************************/
function theme_scripts() {
    //wp_enqueue_style( $handle, $src, $deps, $ver, $media );
    //wp_register_script( $handle, $src, $deps, $ver, $in_footer );

    wp_deregister_script('jquery');// removes jquery so we can deliver it in our own file (Grunt)
    wp_deregister_script('jquery-ui-core');
    wp_deregister_script('jquery-ui-datepicker');
    //wp_deregister_style('gtranslate-style');

    // deregister gravity forms so we can roll these scripts into our own file (Grunt)
    wp_deregister_script('gform_json');
    wp_deregister_script('gform_gravityforms');
    wp_deregister_script('gform_placeholder');
}

add_action('wp_enqueue_scripts', 'theme_scripts', 100);


/***************************************************************
 * Removing unnecessary WP Head stuff
 * ref: https://bhoover.com/remove-unnecessary-code-from-your-wordpress-blog-header/
 ***************************************************************/
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');


/***************************************************************
 * Disable WordPress Update Notifications and Plugin Update Notifications
 ***************************************************************/
//function remove_core_updates(){
//	global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
//}
//add_filter('pre_site_transient_update_core','remove_core_updates');
//add_filter('pre_site_transient_update_plugins','remove_core_updates');
//add_filter('pre_site_transient_update_themes','remove_core_updates');


/***************************************************************
 * General Settings page
 ***************************************************************/
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'General Settings Page',
        'menu_title' => 'General Settings',
        'menu_slug' => 'general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}


/***************************************************************
 * DarkSky.net weather (current conditions) 
 ***************************************************************/

global $forecast;

/* Free Account -- 1000 used per day.   Breaks the Internet when we go over.
 * We will use a WP Transient to store the data.
 * REF: https://darksky.net/dev/docs
 */

$api_key = 'xxxxxxxxxxxxxxxxxxxx';

// Long and Lat
$latitude = '44.5193';
$longitude = '-109.0977';
$units = 'us';  // Can be set to 'us', 'si', 'ca', 'uk' or 'auto' (SEE: https://darksky.net/dev/docs); default is us
$lang = 'en'; // Can be set to 'en', 'de', 'pl', 'es', 'fr', 'it', 'tet' or 'x-pig-latin' (SEE: https://darksky.net/dev/docs); default is 'en'

// Forecast URL
$url = "https://api.darksky.net/forecast/$api_key/$latitude,$longitude?units=$units&lang=$lang";

// We only require Current Conditions - Summary, Icon, Temperature

// Simple DarkSky Forecast Icon
function getWeatherIcon(&$forecast) {
    switch ($forecast['currently']['icon']) {
        case 'clear-day':
            $image = 'wi-day-sunny.svg';
            break;
        case 'clear-night':
            $image = 'wi-night-clear.svg';
            break;
        case 'partly-cloudy-day':
            $image = 'wi-day-cloudy.svg';
            break;
        case 'partly-cloudy-night':
            $image = 'wi-night-cloudy.svg';
            break;
        case 'rain':
            $image = 'wi-rain.svg';
            break;
        case 'sleet':
            $image = 'wi-sleet.svg';
            break;
        case 'snow':
            $image = 'wi-snow.svg';
            break;
        case 'wind':
            $image = 'wi-windy.svg';
            break;
        case 'fog':
            $image = 'wi-fog.svg';
            break;
        case 'hail':
            $image = 'wi-hail.svg';
            break;
        case 'thunderstorm':
            $image = 'wi-thunderstorm.svg';
            break;
        case 'tornado':
            $image = 'wi-tornado.svg';
            break;
        case 'cloudy':
        default:
            $image = 'wi-cloud.svg';
            break;
    }
    return $image;
}

// Get any existing copy of our transient Weather data
if (false === ($forecast = get_transient('forecast'))) {
    // It wasn't there, so regenerate the data and save the transient
    try {
        $result = @file_get_contents($url);  // Note: suppressing errors if we can't fetch it.
        $forecast = json_decode($result, true);
    } catch (Exception $e) {
        // handle the exception
        $forecast = NULL;
    }

    set_transient('forecast', $forecast, 20 * MINUTE_IN_SECONDS);
}

// Create a simple function to delete our Weather transient
function edit_term_delete_transient() {
    delete_transient('forecast');
}

// Add the function to the edit_term hook so it runs when categories/tags are edited
add_action('edit_term', 'edit_term_delete_transient');


/***************************************************************
 * Activity List Sort Order  (by title, ASC)
 ***************************************************************/

function activities_sort_order($query) {
    if (($query->is_main_query()) && (is_tax('activities_category'))) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
    }
}

add_action('pre_get_posts', 'activities_sort_order');
