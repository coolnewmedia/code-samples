<?php

/**
 * Single: Accommodations
 * Custom Template for Accommodation Pages
 *
 */

get_header();

$hero_image = get_field('hero_image');
$default_single_post_hero = get_field('default_single_post_hero_image', 'option');

?>
<?php if (have_posts()) : while (have_posts()) : the_post() ?>

    <div id="hero" class="hero-short">
        <div class="item">
            <?php if ($hero_image) : ?>
                <img src="<?php echo $hero_image['url']; ?>" />
            <?php elseif ($default_single_post_hero) : ?>
                <img src="<?php echo $default_single_post_hero['url']; ?>" />
            <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default-header.jpg" alt="Scenic lake view of the Country with rolling hills and mountains in the distance" />
            <?php endif; ?>
        </div>
    </div>

<article class="single-accommodation <?php if (has_post_thumbnail()) : echo 'has-image'; endif; ?>">
    <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid.png" alt="ripple background" />
    <div class="container">
        <div class="row no-gutters">
            <div class="col-sm-12 col-md-7">
                <h1><?php the_title(); ?></h1>

                <?php if (is_singular( array('activities', 'accommodations', 'transportation'))) : ?>
                    <?php
                        $address = get_field('address');
                        $phone_number = get_field('phone_number');
                        $email_address = get_field('email_address');
                        $website = get_field('website');
                        $details = get_field('details');
                    ?>
                    <div class="contact-info">
                        <?php if ($address) : ?>
                            <div class="address"><span>Address:</span> <?php echo $address; ?></div>
                        <?php endif; ?>

                        <?php if ($phone_number) : ?>
                            <div class="phone-number"><span>Phone Number:</span> <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></div>
                        <?php endif; ?>

                        <?php if ($email_address) : ?>
                            <div><a class="email-address"><span>Email:</span> <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></div>
                        <?php endif; ?>

                        <?php if ($website) : ?>
                            <div class="website"><span>Website:</span> <a href="<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a></div>
                        <?php endif; ?>
                    </div>

                    <?php echo $details; ?>
                <?php endif; ?>
            </div>
            <?php if (has_post_thumbnail()) : ?>
                <div class="col-sm-12 col-md-4 offset-md-1 pr-md-0">
                    <div class="post__image">
                        <?php echo get_the_post_thumbnail(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</article>
<?php endwhile; endif; ?>

<?php include(locate_template('template-parts/related-posts.php', false, false)); ?>

<?php get_footer();