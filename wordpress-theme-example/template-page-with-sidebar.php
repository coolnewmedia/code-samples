<?php
/* Template Name: Basic Page with Sidebar */

get_header();

$content = get_field('content');

?>

    <div class="basic-page-with-sidebar">
        <img class="ripple" src="<?php echo get_template_directory_uri(); ?>/img/white-ripple-solid-2.png" alt="ripple background"/>
        <div class="container single-page">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-9 pr-md-3 pr-lg-5 main-content">
                    <h1><?php the_title(); ?></h1>
                    <div class="content">
                        <?php echo $content; ?>
                    </div>
                </div>
                <button class="mobile-sidebar-toggle btn"
                        type="button"
                        data-toggle="collapse"
                        data-target="#sidebar"
                        aria-expanded="false"
                        aria-controls="sidebar">
                    Navigation
                </button>
                <div class="col-sm-12 col-md-4 col-lg-3 sidebar sidebar-mobile collapse" id="sidebar">
                    <?php echo wpse_get_ancestor_tree(); ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 sidebar sidebar-desktop">
                    <?php echo wpse_get_ancestor_tree(); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();